# Nexctloud Password Client
Currently I am not developing the app.  
At the moment I try to build a CD / CI pipeline to automatically compile the app and publish it to flathub and provide a linux / windows / mac build.  
After that I will work on the users manual.  
After finishing that I am going to create test methods.  
You can find my plans in the [roadmap](https://gitlab.com/j0chn/nextcloud_password_client/-/wikis/Roadmap)
## About the app
Nextcloud Passwords Client is a Flutter based application to manage passwords from the corresponding
[Nextcloud Passwords](https://apps.nextcloud.com/apps/passwords) app.
The focus  is set to desktop clients, especially linux.<p/>
<img src= "assets/npc_logo.png" height="200" width="200"/>
### Screenshots

<img src= "assets/Screenshot_1.png" height="200" width="200"/>
<img src= "assets/Screenshot_2.png" height="200" width="200"/>
<img src= "assets/Screenshot_3.png" height="200" width="200"/>
<img src= "assets/Screenshot_nextcloud_passwords_client_1.png" height="200" width="200"/>
<img src= "assets/Screenshot_nextcloud_passwords_client_2.png" height="200" width="200"/> 
<img src= "assets/Screenshot_nextcloud_passwords_client_3.png" height="200" width="200"/> 
<img src= "assets/Screenshot_nextcloud_passwords_client_4.png" height="200" width="200"/> 
<img src= "assets/Screenshot_nextcloud_passwords_client_5.png" height="200" width="200"/> 
<img src= "assets/Screenshot_nextcloud_passwords_client_6.png" height="200" width="200"/>  

## Worth mentioning
### repo
the folder "snap" is not necessary to build your own app. It is just to let you know which dependencies 
are needed for linux and to make the building/publishing more open
### app handling
#### general
If you check whether the given server URL is valid you are able to enter your credentials and do some first settings.
If you change that URL then, it is not checked again or handled in any way. If you can't login,
because the URL is not reachable you will still get a "credential error".

If you do not save your credentials but your passwords, on the next start a popup will
appear asking for your credentials. Here you are able to change the account but in the first moment you will 
still see the old accounts' passwords / folder.  
Additionally, all your changes to the password are done locally. If you login at a later point, 
your local changes will be overwritten and lost.
#### progress related
If you are coming from a version before December 24th you need to delete the configmodel.hive and configmodel.lock
file within the "your documents"/nextcloud_password_client/ folder. If you do not do this the app won't start.  
Reason is a newly added attribute which has to be persisted.
## Attention
This app is under development and in an early state. A lot of functionality is missing and the UI is not yet optimized.</br>
When using this app you are just able to retrieve your folders and passwords and navigate between them.
Everything else will be implemented little by little.
## Features
### already implemented
- general theme support
- logout function in account settings
- set refresh interval (synchronization of passwords and folders)
- credentials and folders/passwords are encrypted if/when saved locally
- login to unsecured and master password secured instance
- auto login if credentials are saved
- when starting the app, folders and passwords are loaded from storage
- folders and passwords are retrieved in background (refresh rate configurable in settings)

### planned
- provide a settings screen (first steps are done)
- change/delete/create folders and passwords etc.
- implement a splashscreen while loading
- general UI / UX improvements
- extend / correct themes
- load and display icons
- please open an [issue](https://gitlab.com/j0chn/nextcloud-password-client/-/issues)

### known bugs
- masterpassword eventually is not required even you did not save it (I think the session is still valid)
- not really a bug but something I could check at least. The retrieved objects are not stored instantly and you have to wait a moment until they are persisted.
- adding / deleting a tag may cause a displaying / hiding problem because the server requests in background are
  reloading the tag before it is deleted on server or the other way round (actually no bug)
- custom fields of type 'secret' are not displayed
- When text overflows it is not possible to scroll vertically
- sorting of passwords is not persisted and reloaded ond next startup.
- on windows it may happen that you enter two symbols instead of one (eg. {{ instead of {) (seems to be a
problem with desktop support)
- you have to double tap a rows' cell to see password details (this is already [issued](https://github.com/bosskmk/pluto_grid/issues/256))

## Support
Feel free to </br>
<a href="https://www.ko-fi.com/j0chn" target="_blank"><img src="https://cdn.ko-fi.com/cdn/kofi2.png?v=3" alt="Buy Me A Ko-Fi" height="41" width="174"></a>

## Thanks to
- [Jonas Blatt](https://gitlab.com/joleaf/nc-passwords-app/) who provided some templates when I did not progress.  
- Flutter Community supporting me when I stuck 
  - especially Ismail with his helpful attitude and endless patience ;-)
- Marius David Wieschollek helping me with API problems  
- My colleague providing the logo

## Installation
### Linux
1. Install libsecret, libjsoncpp and libsodium and a secret-service (e.g. GnomeKeyring).
2. Maybe you need to symlink the libsodium.so to `/usr/local/lib/libsodium.so`.
3. Download the [prebuild bundle](https://gitlab.com/j0chn/nextcloud_password_client/-/blob/main/debug.tar.gz)
4. Just start the nextcloud_password_client file.

When I get an answer from [flutter-snap-plugin](https://github.com/canonical/flutter-snap/issues/55) developers, I will continue publishing on snap.  
Flatpak publishes are planned but needs a stable release and no developer builds.

### Windows
1. Download and install [Microsoft Visual C++ Redistributable](https://aka.ms/vs/17/release/vc_redist.x64.exe).
2. Download [Libsodium 1.0.18](https://download.libsodium.org/libsodium/releases/libsodium-1.0.18-stable-msvc.zip) and place the `x64/Release/v143/dynamic/libsodium.dll` in your `C:\\Windows\System32` folder.
3. Download the [ZIP](npc_windows_20211213_pbr.zip) and run the `nextcloud_password_client.exe`. 

### Mac OS
1. Install [Libsodium](https://libsodium.gitbook.io/doc/installation)
Download tar ball file and do following  

``` ./configure ```

``` make && make check ```

``` sudo make install ```

2. Donwload [prebuild zip](https://gitlab.com/j0chn/nextcloud_password_client/-/blob/main/macos_prebuild.zip)
3. Extract ZIP and run nextcloud_password_client.app

Maybe you get an error when running the app. I am finding out how to avoid that.
## Build your own / contribute
When pulling / checking out the project please remove the os related folder only if you can't build the project.   
If you have to recreate the OS folder please use  
``` flutter create --platforms=windows . ```  
within the project folder. Use windows or linux or macos depending on your system. 

There are changes you need in order to use the bitdojo plugin for the menu bar.   
If you do not want to use provided OS folders please remember to implement the changes mentioned in
[bitdojos-windows plugin](https://pub.dev/packages/bitsdojo_window).   

If you want to build the app on your own you have to install flutter on your machine.
To do so you can follow [official documentation](https://flutter.dev/docs/get-started/install).
The flutter snap package may cause errors at compile time, in this case use the zip version.

On Linux you will have to install libsecret, libjsoncpp and libsodium (all including development files) and a secret-service.
Libsodium needs to be available at `/usr/local/lib/libsodium.so`. (Symlink is sufficient)
I got it running with [gnome-keyring](https://wiki.gnome.org/Projects/GnomeKeyring),
but here you have to reenter the password after booting the OS once, 
so maybe that popup appears when starting the app. Or you use [QtKeycahin-qt6](https://github.com/frankosterfeld/qtkeychain) 
with [Kwallet](https://community.kde.org/Frameworks).  
But here I got the problem that it did not work after rebooting linux anymore.  
You can check it [here](https://wiki.gnome.org/Projects/Libsecret). You need it to get the package 
[flutter_secure_storage](https://pub.dev/packages/flutter_secure_storage) running 
which is used to store your credentials and passwords / folders securely.  

After you got your machine running and pulled the repo you need to do following commands:  
Enabling Desktop Support:  

``` flutter config enable-linux-desktop ```  

``` flutter config enable-macos-desktop ```  

``` flutter config enable-windows-desktop ```

``` flutter config enable-windows-uwp-desktop ```

Updating flutter packages:  

```flutter pub get```  

running the app:  

```flutter run -d linux```  

```flutter run -d macos```  

```flutter run -d windows```

Building the app:   

```flutter build -d linux```

```flutter build -d macos```

```flutter build -d windows```

