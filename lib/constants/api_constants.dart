const apiProtocol = 'https://';
const apiSessionRequest = '/index.php/apps/passwords/api/1.0/session/request';
const apiSessionOpen = '/index.php/apps/passwords/api/1.0/session/open';
const apiSessionKeepAlive =
    '/index.php/apps/passwords/api/1.0/session/keepalive';
const apiGetFolderList = '/index.php/apps/passwords/api/1.0/folder/list';
const apiGetPasswordList = '/index.php/apps/passwords/api/1.0/password/list';
const apiGetPasswordShow = '/index.php/apps/passwords/api/1.0/password/show';
const apiGetTagList = '/index.php/apps/passwords/api/1.0/tag/list';
const apiPasswordDetail = 'model+revisions+tag-ids+shares';
const apiPasswordUpdate = '/index.php/apps/passwords/api/1.0/password/update';
const apiTagCreate = '/index.php/apps/passwords/api/1.0/tag/create';
const apiTagShow = '/index.php/apps/passwords//api/1.0/tag/show';
const apiGetServerSettingsList =
    '/index.php/apps/passwords/api/1.0/settings/list';
const apiGetFavIcon =
    '/index.php/apps/passwords/api/1.0/service/favicon/{domain}/32';
const apiChallenge = 'challenge';
const apiType = 'type';
const apiPWDv1r1 = 'PWDv1r1';
const apiSalts = 'salts';
const apiToken = 'token';
const apiSuccess = 'success';
const apiRootFolder = '00000000-0000-0000-0000-000000000000';
