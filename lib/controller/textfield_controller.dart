// Flutter imports:
import 'package:flutter/material.dart';

class TextFieldController {
  static TextEditingController urlTextFieldController = TextEditingController();
  static TextEditingController userNameFieldController =
      TextEditingController();
  static TextEditingController passwordFieldController =
      TextEditingController();
  static TextEditingController masterPasswordFieldController =
      TextEditingController();
  static TextEditingController clientPasswordFieldController =
      TextEditingController();
  static TextEditingController passwordController = TextEditingController();
  static TextEditingController refreshRateController = TextEditingController();
  static TextEditingController searchUserTextController =
      TextEditingController();
  static TextEditingController tagController = TextEditingController();
}
