// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'nodes.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NodesAdapter extends TypeAdapter<Nodes> {
  @override
  final int typeId = 12;

  @override
  Nodes read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Nodes.all;
      case 1:
        return Nodes.folder;
      case 2:
        return Nodes.recent;
      case 3:
        return Nodes.shared;
      case 4:
        return Nodes.security;
      case 5:
        return Nodes.tag;
      case 6:
        return Nodes.favorite;
      default:
        return Nodes.all;
    }
  }

  @override
  void write(BinaryWriter writer, Nodes obj) {
    switch (obj) {
      case Nodes.all:
        writer.writeByte(0);
        break;
      case Nodes.folder:
        writer.writeByte(1);
        break;
      case Nodes.recent:
        writer.writeByte(2);
        break;
      case Nodes.shared:
        writer.writeByte(3);
        break;
      case Nodes.security:
        writer.writeByte(4);
        break;
      case Nodes.tag:
        writer.writeByte(5);
        break;
      case Nodes.favorite:
        writer.writeByte(6);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NodesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
