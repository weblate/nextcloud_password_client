// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'security.g.dart';

@HiveType(typeId: 11)
enum Security {
  @HiveField(0)
  secure,
  @HiveField(1)
  weak,
  @HiveField(2)
  breached,
}
