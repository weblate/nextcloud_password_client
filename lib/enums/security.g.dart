// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'security.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SecurityAdapter extends TypeAdapter<Security> {
  @override
  final int typeId = 11;

  @override
  Security read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Security.secure;
      case 1:
        return Security.weak;
      case 2:
        return Security.breached;
      default:
        return Security.secure;
    }
  }

  @override
  void write(BinaryWriter writer, Security obj) {
    switch (obj) {
      case Security.secure:
        writer.writeByte(0);
        break;
      case Security.weak:
        writer.writeByte(1);
        break;
      case Security.breached:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SecurityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
