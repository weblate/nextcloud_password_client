// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

class CredentialsException extends GeneralException {
  CredentialsException(String msg, int errorCode) : super(msg, errorCode);

  @override
  String toString() {
    return "Message : $msg, Error code $errorCode";
  }
}
