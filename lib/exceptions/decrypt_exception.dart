// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

class DecryptException extends GeneralException {
  DecryptException(String msg, int errorCode) : super(msg, errorCode);

  @override
  String toString() {
    return msg;
  }
}
