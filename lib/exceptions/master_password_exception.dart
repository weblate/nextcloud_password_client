// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

class MasterPasswordException extends GeneralException {
  MasterPasswordException(String msg, int errorCode) : super(msg, errorCode);

  @override
  String toString() {
    return "Message : $msg, Error code $errorCode";
  }
}
