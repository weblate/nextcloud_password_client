// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

class MissingCredentialsException extends GeneralException {
  MissingCredentialsException(String msg, int errorCode)
      : super(msg, errorCode);

  @override
  String toString() {
    return "Message : $msg, Error code $errorCode";
  }
}
