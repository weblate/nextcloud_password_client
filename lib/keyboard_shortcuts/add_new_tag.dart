// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final addNewTagKeySet = LogicalKeySet(
  LogicalKeyboardKey.enter,
);

class AddNewTagIntent extends Intent {}

class AddNewTagShortcut extends StatelessWidget {
  const AddNewTagShortcut({
    required Key key,
    required this.child,
    required this.onAddNewTagDetected,
  }) : super(key: key);
  final Widget child;
  final VoidCallback onAddNewTagDetected;

  @override
  Widget build(BuildContext context) {
    return FocusableActionDetector(
      autofocus: true,
      shortcuts: {
        addNewTagKeySet: AddNewTagIntent(),
      },
      actions: {
        AddNewTagIntent:
            CallbackAction(onInvoke: (e) => onAddNewTagDetected.call()),
      },
      child: child,
    );
  }
}
