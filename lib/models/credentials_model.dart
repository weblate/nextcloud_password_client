// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'credentials_model.g.dart';

@HiveType(typeId: 5)
class CredentialsModel extends ChangeNotifier {
  @HiveField(0)
  String userName = '';
  @HiveField(1)
  String password = '';
  @HiveField(2)
  String masterPassword = '';
  @HiveField(3)
  String clientPassword = '';

  CredentialsModel();
}
