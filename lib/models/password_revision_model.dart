// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'password_revision_model.g.dart';

@HiveType(typeId: 10)
class PasswordRevisionModel extends ChangeNotifier {
  PasswordRevisionModel();
  @HiveField(0)

  /// The UUID of the password
  String id = '';
  @HiveField(1)

  /// User defined label of the password
  String label = '';
  @HiveField(2)

  /// Username associated with the password
  String username = '';
  @HiveField(3)

  /// The actual password
  String password = '';
  @HiveField(4)

  ///Url of the website
  String url = '';
  @HiveField(5)

  /// Notes for the password. Can be formatted with Markdown
  String notes = '';
  @HiveField(6)

  /// Custom fields created by the user. (See custom fields)
  String customFields = '';
  @HiveField(7)

  /// Security status level of the password (0 = ok, 1 = user rules violated, 2 = breached)
  int status = 0;
  @HiveField(8)

  /// Specific code for the current security status (GOOD, OUTDATED, DUPLICATE, BREACHED)
  String statusCode = '';
  @HiveField(9)

  /// SHA1 hash of the password
  String hash = '';
  @HiveField(10)

  /// UUID of the current folder of the password
  String folder = '';
  @HiveField(11)

  /// Type of the used client side encryption
  String cseType = '';
  @HiveField(12)

  /// UUID of the key used for client side encryption
  String cseKey = '';
  @HiveField(13)

  /// Type of the used server side encryption
  String sseType = '';
  @HiveField(14)

  /// Name of the client which created this revision
  String client = '';
  @HiveField(15)

  /// Hides the password in list / find actions
  bool hidden = false;
  @HiveField(16)

  /// True if the password is in the trash
  bool trashed = false;
  @HiveField(17)

  /// True if the user has marked the password as favorite
  bool favorite = false;
  @HiveField(18)

  /// Unix timestamp when the user last changed the password
  int edited = 0;
  @HiveField(19)

  /// Unix timestamp when the password was created
  int created = 0;
  @HiveField(20)

  /// Unix timestamp when the password was updated
  int updated = 0;
}
