// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'password_revision_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PasswordRevisionModelAdapter extends TypeAdapter<PasswordRevisionModel> {
  @override
  final int typeId = 10;

  @override
  PasswordRevisionModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PasswordRevisionModel()
      ..id = fields[0] as String
      ..label = fields[1] as String
      ..username = fields[2] as String
      ..password = fields[3] as String
      ..url = fields[4] as String
      ..notes = fields[5] as String
      ..customFields = fields[6] as String
      ..status = fields[7] as int
      ..statusCode = fields[8] as String
      ..hash = fields[9] as String
      ..folder = fields[10] as String
      ..cseType = fields[11] as String
      ..cseKey = fields[12] as String
      ..sseType = fields[13] as String
      ..client = fields[14] as String
      ..hidden = fields[15] as bool
      ..trashed = fields[16] as bool
      ..favorite = fields[17] as bool
      ..edited = fields[18] as int
      ..created = fields[19] as int
      ..updated = fields[20] as int;
  }

  @override
  void write(BinaryWriter writer, PasswordRevisionModel obj) {
    writer
      ..writeByte(21)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.label)
      ..writeByte(2)
      ..write(obj.username)
      ..writeByte(3)
      ..write(obj.password)
      ..writeByte(4)
      ..write(obj.url)
      ..writeByte(5)
      ..write(obj.notes)
      ..writeByte(6)
      ..write(obj.customFields)
      ..writeByte(7)
      ..write(obj.status)
      ..writeByte(8)
      ..write(obj.statusCode)
      ..writeByte(9)
      ..write(obj.hash)
      ..writeByte(10)
      ..write(obj.folder)
      ..writeByte(11)
      ..write(obj.cseType)
      ..writeByte(12)
      ..write(obj.cseKey)
      ..writeByte(13)
      ..write(obj.sseType)
      ..writeByte(14)
      ..write(obj.client)
      ..writeByte(15)
      ..write(obj.hidden)
      ..writeByte(16)
      ..write(obj.trashed)
      ..writeByte(17)
      ..write(obj.favorite)
      ..writeByte(18)
      ..write(obj.edited)
      ..writeByte(19)
      ..write(obj.created)
      ..writeByte(20)
      ..write(obj.updated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PasswordRevisionModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
