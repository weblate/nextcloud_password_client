// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'password_share_model.g.dart';

@HiveType(typeId: 8)
class PasswordShareModel extends ChangeNotifier {
  /// The UUID of the share
  @HiveField(0)
  String id = '';

  /// Unix timestamp when the share was created
  @HiveField(1)
  int created = 000000000000000;

  /// Unix timestamp when the share was updated
  @HiveField(2)
  int updated = 000000000000000;

  /// Unix timestamp when the share will expire
  @HiveField(3)
  int expires = 000000000000000;

  /// Whether or not the receiving user can edit the password
  @HiveField(4)
  bool editable = true;

  /// Whether or not the receiving user can share the password again
  @HiveField(5)
  bool shareable = true;

  /// Whether or not data changes for this share are in queue
  @HiveField(6)
  bool updatePending = false;

  /// The UUID of the password
  @HiveField(7)
  String password = '';

  /// Object with the id and the full name of the owner
  @HiveField(8)
  dynamic owner = '';

  /// Object with the id and the full name of the receiver
  @HiveField(9)
  dynamic receiver = '';

  PasswordShareModel();
}
