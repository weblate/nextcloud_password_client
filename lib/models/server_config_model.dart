// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'server_config_model.g.dart';

@HiveType(typeId: 7)
class ServerConfigModel extends ChangeNotifier {
  @HiveField(0)

  ///  The strength of generated passwords, a value between 0 (weak) and 4 (strong)
  int userPasswordGeneratorStrength = 1;
  @HiveField(1)

  ///Whether or not generated passwords contain numbers
  bool userPasswordGeneratorNumbers = false;
  @HiveField(2)

  ///Whether or not generated passwords contain special characters
  bool userPasswordGeneratorSpecial = false;
  @HiveField(3)

  ///Whether or not the server should check passwords for duplicates
  bool usePasswordSecurityDuplicates = true;
  @HiveField(4)

  /// Marks passwords a weak if they surpass the specified date mark. 0 is off.
  int usePasswordSecurityAge = 0;
  @HiveField(5)

  ///How many characters of the has should be saved. Can be either 0, 20, 30 or 40. For e2e this is implemented by the client, count characters from the beginning of the string
  int userPasswordSecurityHash = 40;
  @HiveField(6)

  /// Whether or not the user receives mails about security issues
  bool useMailSecurity = true;
  @HiveField(7)

  ///Whether or not the user receives mails about new shared objects
  bool userMailShares = false;
  @HiveField(8)

  /// Whether or not the user receives notifications about security issues
  bool userNotificationSecurity = true;
  @HiveField(9)

  ///Whether or not the user receives notifications about new shared objects
  bool userNotificationShares = true;
  @HiveField(10)

  ///Whether or not the user receives notifications about background errors
  bool userNotificationErrors = true;
  @HiveField(11)

  ///Whether or not the user receives notifications about configuration issues. Only for admins
  bool userNotificationAdmin = true;
  @HiveField(12)

  ///The server side encryption type. 0 = None, 1 = SSEv1, 2 = SSEv1
  int userEncryptionSse = 1;
  @HiveField(13)

  ///The client side encryption type. 0 = None, 1 = CSEv1
  int userEncryptionCse = 0;
  @HiveField(14)

  ///Whether or not the editable option should be set by default for a new share. Not writeable if sharing disabled.
  bool userSharingEditable = true;
  @HiveField(15)

  ///Whether or not the resharing option should be set by default for a new share. Not writeable if sharing or resharing disabled.
  bool userSharingResharing = true;
  @HiveField(16)

  ///The session lifetime in seconds
  int userSessionLifetime = 600;
  @HiveField(17)

  ///The Nextcloud version of the server, e.g. "20"
  String serverVersion = '';
  @HiveField(18)

  ///The app version of the server, e.g. "2020.9"
  String serverAppVersion = '';
  @HiveField(19)

  ///The base url of the server
  String serverBaseUrl = '';
  @HiveField(20)

  ///The base url of the WebDav service used for file storage
  String serverBaseUrlWebdav = '';
  @HiveField(21)

  ///Whether or not sharing is enabled globally
  bool serverSharingEnabled = true;
  @HiveField(22)

  ///Whether or not resharing shared entities is enabled globally
  bool serverSharingResharing = true;
  @HiveField(23)

  ///Whether or not the auto-complete request works
  bool serverSharingAutocomplete = true;
  @HiveField(24)

  ///List of supported sharing types.
  List<String> serverSharingTypes = ["user"];
  @HiveField(25)

  ///The color of the current Nextcloud theme
  String serverThemeColorPrimary = '#745bca';
  @HiveField(26)

  ///The text color of the current Nextcloud theme
  String serverThemeColorText = '#ffffff';
  @HiveField(27)

  ///The background color of the current Nextcloud theme
  String serverThemeColorBackground = '#ffffff';
  @HiveField(28)

  ///The url to the current Nextcloud background image
  String serverThemeBackground = '';
  @HiveField(29)

  ///The url to the logo of the current Nextcloud theme
  String serverThemeLogo = '';
  @HiveField(30)

  ///The name of the Nextcloud instance
  String serverThemeLabel = "Nextcloud";
  @HiveField(31)

  ///The url to the current svg app icon
  String serverThemeAppIcon = '';
  @HiveField(32)

  ///The url to the current svg folder icon
  String serverThemeFolderIcon = '';
  @HiveField(33)

  ///The base url of the in-app user handbook
  String serverHandbookUrl = '';
  @HiveField(34)

  ///An integer value indicating the server performance preference. 0 is a slow server where requests should be avoided (max 1 simultaneous request), 5 is a fast server which can handle many requests (x * 3 simultaneous requests), 6 is for unlimited requests.
  int serverPerformance = 2;
  ServerConfigModel();
  ServerConfigModel.fromMap(Map<String, dynamic> serverConfig) {
    userPasswordGeneratorStrength =
        serverConfig['user.password.generator.strength'];
    userPasswordGeneratorNumbers =
        serverConfig['user.password.generator.numbers'];
    userPasswordGeneratorSpecial =
        serverConfig['user.password.generator.special'];
    usePasswordSecurityDuplicates =
        serverConfig['user.password.security.duplicates'];
    usePasswordSecurityAge = serverConfig['user.password.security.age'];
    usePasswordSecurityAge = serverConfig['user.password.security.age'];
    userPasswordSecurityHash = serverConfig['user.password.security.hash'];
    useMailSecurity = serverConfig['user.mail.security'];
    userMailShares = serverConfig['user.mail.shares'];
    userNotificationSecurity = serverConfig['user.notification.security'];
    userNotificationShares = serverConfig['user.notification.shares'];
    userNotificationErrors = serverConfig['user.notification.errors'];
    userNotificationAdmin = serverConfig['user.notification.admin'];
    userEncryptionSse = serverConfig['user.encryption.sse'];
    userEncryptionCse = serverConfig['user.encryption.cse'];
    userSharingEditable = serverConfig['user.sharing.editable'];
    userSharingResharing = serverConfig['user.sharing.resharing'];
    userSessionLifetime = serverConfig['user.session.lifetime'];
    serverVersion = serverConfig['server.version'];
    serverAppVersion = serverConfig['server.app.version'];
    serverBaseUrl = serverConfig['server.baseUrl'];
    serverBaseUrlWebdav = serverConfig['server.baseUrl.webdav'];
    serverSharingEnabled = serverConfig['server.sharing.enabled'];
    serverSharingResharing = serverConfig['server.sharing.resharing'];
    serverSharingAutocomplete = serverConfig['server.sharing.autocomplete'];
    for (dynamic item in serverConfig['server.sharing.types']) {
      serverSharingTypes.add(item);
    }
    serverThemeColorPrimary = serverConfig['server.theme.color.primary'];
    serverThemeColorText = serverConfig['server.theme.color.text'];
    serverThemeColorBackground = serverConfig['server.theme.color.background'];
    serverThemeBackground = serverConfig['server.theme.background'];
    serverThemeLogo = serverConfig['server.theme.logo'];
    serverThemeLabel = serverConfig['server.theme.label'];
    serverThemeAppIcon = serverConfig['server.theme.app.icon'];
    serverThemeFolderIcon = serverConfig['server.theme.folder.icon'];
    serverHandbookUrl = serverConfig['server.handbook.url'];
    serverPerformance = serverConfig['server.performance'];
  }
}
