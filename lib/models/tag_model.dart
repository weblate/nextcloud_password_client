// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';

part 'tag_model.g.dart';

@HiveType(typeId: 9)
class TagModel extends BaseModel {
  /// The color of the tag. Any valid CSSv3 color is accepted.
  @HiveField(13)
  String color = '#ffffff';

  TagModel();
}
