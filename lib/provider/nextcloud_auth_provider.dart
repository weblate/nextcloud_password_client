// Dart imports:
import 'dart:convert';
import 'dart:typed_data';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:http/http.dart' as http;
import 'package:libsodium/libsodium.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/constants/exception_constants.dart';
import 'package:nextcloud_password_client/constants/http_constants.dart';
import 'package:nextcloud_password_client/exceptions/credentials_exception.dart';
import 'package:nextcloud_password_client/exceptions/master_password_exception.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';

class NextcloudAuthProvider {
  static NextcloudAuthProvider? _instance;

  NextcloudAuthProvider._internal();

  factory NextcloudAuthProvider() {
    _instance ??= NextcloudAuthProvider._internal();
    return _instance!;
  }

  String _serverUrl = '';
  String _username = '';
  String _password = '';
  String _masterPassword = '';
  late KeyChain _keyChain;
  late String _session;
  late BuildContext _context;

  get keyChain => _keyChain;

  Future<void> handleLogin(BuildContext context) async {
    _context = context;
    CredentialsViewModel credentialsViewModel =
        context.read<CredentialsViewModel>();
    ConfigViewModel configViewModel = context.read<ConfigViewModel>();
    _serverUrl = context.read<ConfigViewModel>().serverURL;
    _username = credentialsViewModel.userName;
    _password = credentialsViewModel.password;
    _keyChain = configViewModel.keyChain;
    _masterPassword = credentialsViewModel.masterPassword;
    _session = configViewModel.session;
    await _requestSession(_serverUrl);
  }

  Future<void> _requestSession(String serverUrl) async {
    HttpUtils.setLoginData(_serverUrl, _username, _password, _session);
    http.Response _requestSessionResponse =
        await HttpUtils.httpGet(apiSessionRequest);
    final _apiResponse =
        json.decode(_requestSessionResponse.body) as Map<String, dynamic>;
    if (_requestSessionResponse.statusCode == successStatus) {
      await _handleSessionRequestResponse(_apiResponse);
    } else {
      throw CredentialsException(credentialsdError, credentialErrorCode);
    }
  }

  Future<void> _handleSessionRequestResponse(
      Map<String, dynamic> _apiResponse) async {
    if (_apiResponse.isEmpty) {
    } else if (_apiResponse.containsKey(apiChallenge)) {
      await _handleChallenge(_apiResponse[apiChallenge]);
    } else if (_apiResponse.containsKey(apiToken)) {
      //TODO:handle token here
    }
  }

  Future<void> _handleChallenge(dynamic _challengeValues) async {
    if (_challengeValues[apiType] == apiPWDv1r1) {
      await _handlePWDV1(_challengeValues[apiSalts]);
    }
  }

  Future<void> _handlePWDV1(dynamic _salts) async {
    if (_masterPassword.isEmpty) {
      throw MasterPasswordException(
          masterPassworMissingdError, masterpasswordMissingErrorCode);
    } else {
      final _p = utf8.encode(_masterPassword);
      final _salt0 = Sodium.hex2bin(_salts[0]);
      final _salt1 = Sodium.hex2bin(_salts[1]);
      final _salt2 = Sodium.hex2bin(_salts[2]);
      final x = Uint8List.fromList(_p + _salt0);
      final _geneticHash =
          Sodium.cryptoGenerichash(Sodium.cryptoGenerichashBytesMax, x, _salt1);
      final _passwordHash = Sodium.cryptoPwhash(
        Sodium.cryptoBoxSeedbytes,
        _geneticHash,
        _salt2,
        Sodium.cryptoPwhashOpslimitInteractive,
        Sodium.cryptoPwhashMemlimitInteractive,
        Sodium.cryptoPwhashAlgDefault,
      );
      final _secret = Sodium.bin2hex(_passwordHash);
      http.Response _challengeResponse = await HttpUtils.httpPost(
        apiSessionOpen,
        body: jsonEncode({apiChallenge: _secret}),
      );
      if (_challengeResponse.statusCode == 200 &&
          json.decode(_challengeResponse.body)[apiSuccess]) {
        _session = _context.read<ConfigViewModel>().session =
            _challengeResponse.headers['x-api-session'] as String;
        HttpUtils.setLoginData(_serverUrl, _username, _password, _session);
        _handleCSEv1r1(json.decode(_challengeResponse.body)['keys']['CSEv1r1']);
        _keepSessionAlive();
      } else {
        throw MasterPasswordException(
            masterPasswordWrongError, masterpasswordWrongErrorCode);
      }
    }
  }

  void _handleCSEv1r1(String _csev1r1) {
    final _p = utf8.encode(_masterPassword) as Uint8List;
    final _key = _csev1r1;
    final _keyE = Sodium.hex2bin(_key);
    final _keySalt = _keyE.sublist(0, Sodium.cryptoPwhashSaltbytes);
    final _keyPayload = _keyE.sublist(Sodium.cryptoPwhashSaltbytes);
    final _decryptionKey = Sodium.cryptoPwhash(
      Sodium.cryptoBoxSeedbytes,
      _p,
      _keySalt,
      Sodium.cryptoPwhashOpslimitInteractive,
      Sodium.cryptoPwhashMemlimitInteractive,
      Sodium.cryptoPwhashAlgDefault,
    );
    final _nonce = _keyPayload.sublist(0, Sodium.cryptoSecretboxNoncebytes);
    final _cipher = _keyPayload.sublist(Sodium.cryptoSecretboxNoncebytes);
    final _keychainObject =
        Sodium.cryptoSecretboxOpenEasy(_cipher, _nonce, _decryptionKey);
    final _keyChainJsonString = utf8.decode(_keychainObject);
    _keyChain = _context.read<ConfigViewModel>().keyChain =
        KeyChain.fromMap(json.decode(_keyChainJsonString));
  }

  void _keepSessionAlive() {
    Future.delayed(
      const Duration(seconds: 40),
      () async {
        await HttpUtils.httpGet(apiSessionKeepAlive);
        if (_session != '') _keepSessionAlive();
      },
    );
  }
}
