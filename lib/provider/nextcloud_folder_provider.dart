// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';

class NextcloudFolderProvider {
  static Future<Map<String, List<FolderViewModel>>> retrieveFolders() async {
    http.Response _requestSessionResponse =
        await HttpUtils.httpGet(apiGetFolderList);
    var body = json.decode(_requestSessionResponse.body);
    Map<String, List<FolderViewModel>> models = {};
    for (Map<String, dynamic> folder in body) {
      FolderViewModel fvm = FolderViewModel.fromMap(folder);
      if (!models.containsKey(fvm.parent)) {
        models[fvm.parent] = [];
      }
      models[fvm.parent]!.add(fvm);
    }
    return models;
  }
}
