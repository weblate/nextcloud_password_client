// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';

class NextcloudPasswordProvider {
  static Future<Map<String, List<PasswordViewModel>>>
      retrievePasswords() async {
    http.Response _passwordSessionResponse = await HttpUtils.httpPost(
        apiGetPasswordList,
        body: jsonEncode({'details': apiPasswordDetail}));
    var body = json.decode(_passwordSessionResponse.body);
    Map<String, List<PasswordViewModel>> models = {};
    for (Map<String, dynamic> password in body) {
      PasswordViewModel pvm = PasswordViewModel.fromMap(password);
      /*  String favIconUrl =
            url + apiGetFavIcon.replaceAll('{domain}', Uri.parse(pwUrl).host);
        http.Response favIconResponse =
            await HttpUtils.httpGet(favIconUrl, header);
        if (favIconResponse.statusCode == 200) {
          pvm.favIconString = base64.encode(favIconResponse.bodyBytes);
        }
      }*/
      if (!models.containsKey(pvm.folder)) {
        models[pvm.folder] = [];
      }
      models[pvm.folder]!.add(pvm);
    }
    return models;
  }
}
