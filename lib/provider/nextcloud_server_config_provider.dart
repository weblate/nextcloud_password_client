// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';

class NextcloudServerConfigProvider {
  static Future<ServerConfigModel> retrieveServerConfig() async {
    http.Response _requestSessionResponse =
        await HttpUtils.httpGet(apiGetServerSettingsList);
    var body = json.decode(_requestSessionResponse.body);
    return ServerConfigModel.fromMap(body);
  }
}
