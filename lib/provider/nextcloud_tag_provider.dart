// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class NextcloudTagProvider {
  static Future<List<TagViewModel>> retrieveTags() async {
    http.Response _requestSessionResponse =
        await HttpUtils.httpGet(apiGetTagList);
    var body = json.decode(_requestSessionResponse.body);
    List<TagViewModel> models = [];
    for (Map<String, dynamic> tag in body) {
      TagViewModel tvm = TagViewModel.fromMap(tag);
      models.add(tvm);
    }
    return models;
  }
}
