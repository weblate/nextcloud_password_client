// Project imports:
import 'package:nextcloud_password_client/enums/routing_path.dart';
import 'package:nextcloud_password_client/router/page_action.dart';

class PageConfiguration {
  final String key;
  final String path;
  final RoutingPath uiPage;
  PageAction? currentPageAction;

  PageConfiguration(
      {required this.key,
      required this.path,
      required this.uiPage,
      this.currentPageAction});
}
