// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/page_config_constants.dart';
import 'package:nextcloud_password_client/constants/routing_paths_constants.dart';
import 'package:nextcloud_password_client/router/page_configuration.dart';

class PasswordRouteParser extends RouteInformationParser<PageConfiguration> {
  @override
  Future<PageConfiguration> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location as String);
    if (uri.pathSegments.isEmpty) {
      return loginPageConfig;
    }

    final path = uri.pathSegments[0];
    switch (path) {
      case routingPathLogin:
        return loginPageConfig;
      case routingPathSettings:
        return settingsPageConfig;
      case routingPathPasswordScreen:
        return passwordScreenPageConfig;
      default:
        return loginPageConfig;
    }
  }
}
