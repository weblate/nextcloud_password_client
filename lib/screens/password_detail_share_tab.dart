// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

Widget getPasswordShareTab(BuildContext context, PasswordViewModel password) {
  String owner = '';
  if (password.passwordShare.owner.toString().isNotEmpty) {
    owner = password.passwordShare.owner['name'];
  }
  return //Expanded(    child:
      Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          owner.toString().isNotEmpty
              ? SizedBox(
                  child: Text(password.passwordShare.owner['name'] +
                      ' shared this password with you'),
                  width: 400,
                  height: 30,
                )
              : const SizedBox.shrink(),
        ],
      ),
      SizedBox(
        child: getGeneralTextInput(TextFieldController.userNameFieldController,
            label: 'Benutzer suchen'),
        width: 400,
      ),
      Expanded(
        child: ListView.builder(
          itemCount: password.shareViewModels.length,
          itemBuilder: (BuildContext context, int index) {
            PasswordShareViewModel passwordShareViewModel =
                password.shareViewModels.elementAt(index);
            return ListTile(
              hoverColor: Colors.black12,
              onTap: () {},
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    passwordShareViewModel.receiver['name'],
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.create_rounded)),
                        IconButton(
                            onPressed: () {}, icon: const Icon(Icons.share)),
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.calendar_today)),
                        IconButton(
                            onPressed: () {}, icon: const Icon(Icons.delete))
                      ],
                    ),
                  ),
                ],
              ),
            );
            //onTap: () => context.read<ViewState>().selectedSettingsItem =
            //passwordViewModel.revisions.elementAt(index),
          },
        ),
      ),
    ],
  );
  //);
}
