// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/widgets/navigation_tree.dart';

Widget passwordFolderScreen(BuildContext context) {
  return context.watch<FolderListViewModel>().folderViewModels.isEmpty
      ? const CircularProgressIndicator()
      : createTree(context);
}
