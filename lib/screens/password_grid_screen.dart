// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pluto_grid/pluto_grid.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_column.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_row.dart';

Widget passwordGridScreen(BuildContext context) {
  return context.watch<PasswordListViewModel>().passwordViewModels.isEmpty
      ? const CircularProgressIndicator()
      : PlutoGrid(
          onRowChecked: (event) => context.read<ViewState>().selectedPassword =
              event.row!.key.toString(),
          onSelected: (PlutoGridOnSelectedEvent event) {
            ValueKey<String> key = event.row!.key as ValueKey<String>;
            context.read<ViewState>().selectedPassword = key.value;
          },
          onLoaded: (PlutoGridOnLoadedEvent event) {
            event.stateManager!.setSelectingMode(PlutoGridSelectingMode.none);
            PasswordListViewModel passwordListViewModel =
                context.read<PasswordListViewModel>();
            passwordListViewModel.plutoStateManager = event.stateManager!;
            passwordListViewModel.setPasswordsInitially(context);
            PlutoRow? plutoRow = passwordListViewModel
                .getRowByPassword(context.read<ViewState>().selectedPassword);
            if (plutoRow != null) {
              event.stateManager!.setCurrentCell(
                  plutoRow.cells.values.first, plutoRow.state.index);
            }
          },
          columns: getPasswordHeader(context),
          rows: getPasswordRows(context),
          mode: PlutoGridMode.select,
          configuration:
              context.read<ConfigViewModel>().selectedTheme == Themes.dark
                  ? PlutoGridConfiguration.dark()
                  : null,
        );
}
