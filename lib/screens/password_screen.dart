// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:multi_split_view/multi_split_view.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/provider/nextcloud_provider.dart';
import 'package:nextcloud_password_client/screens/password_detail_screen.dart';
import 'package:nextcloud_password_client/screens/password_folder_screen.dart';
import 'package:nextcloud_password_client/screens/password_grid_screen.dart';
import 'package:nextcloud_password_client/screens/window_border.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/dialogues/client_password_dialogue.dart';

class PasswordScreen extends StatefulWidget {
  const PasswordScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  MultiSplitViewController? folderSplitController;
  List<LogicalKeyboardKey> shortcut = [];
  MultiSplitViewController? passwordSplitController;
  @override
  void initState() {
    RawKeyboard.instance.addListener(_handleKeyDown);
    ViewState viewState = context.read<ViewState>();
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      if (context.read<CredentialsViewModel>().clientPassword.isNotEmpty) {
        if (!viewState.clientAuthenticated) {
          await showClientPasswordDialog(context);
        }
      }
      if (viewState.relogin) {
        await relogin(context);
      }
      NextcloudProvider.retrieveObjects(
          context, context.read<ConfigViewModel>().refreshRateInSeconds);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (folderSplitController == null) {
      folderSplitController = MultiSplitViewController(
          weights: context.read<ViewState>().folderSplitWeights);
      passwordSplitController = MultiSplitViewController(
          weights: context.read<ViewState>().passwordSplitWeights);
    }

    MultiSplitView passwordSplitView = MultiSplitView(
      onSizeChange: (int childIndex1, int childIndex2) =>
          _savePasswordSplitWeights(childIndex1, childIndex2, context),
      minimalWeight: 0.1,
      minimalSize: 10,
      axis: Axis.vertical,
      controller: passwordSplitController,
      children: [
        Center(
          child: passwordGridScreen(context),
        ),
        Center(
          child: passwordDetailScreen(context),
        ),
      ],
    );

    MultiSplitView folderSplit = MultiSplitView(
      controller: folderSplitController,
      minimalWeight: 0.1,
      minimalSize: 10,
      onSizeChange: (int childIndex1, int childIndex2) =>
          _saveFolderSplitWeights(childIndex1, childIndex2, context),
      children: [passwordFolderScreen(context), passwordSplitView],
    );

    MultiSplitViewTheme theme = MultiSplitViewTheme(
        child: folderSplit,
        data: MultiSplitViewThemeData(
            dividerThickness: 1,
            dividerPainter: DividerPainters.background(color: Colors.white)));

    return context.watch<ViewState>().clientAuthenticated ||
            context.read<CredentialsViewModel>().clientPassword.isEmpty
        ? Scaffold(appBar: getWindowBorder(context, true), body: theme)
        : Scaffold(
            appBar: getWindowBorder(context, true),
            body: const SizedBox.shrink());
  }

  void _handleKeyDown(RawKeyEvent value) {
    if (value is RawKeyDownEvent) {
      final k = value.logicalKey;
      shortcut.add(k);
    }

    if (value is RawKeyUpEvent) {
      final k = value.logicalKey;
      shortcut.remove(k);
    }
    if ((shortcut.contains(LogicalKeyboardKey.controlLeft) ||
            shortcut.contains(LogicalKeyboardKey.controlRight)) &&
        shortcut.contains(LogicalKeyboardKey.keyC)) {
      String passwordID = context.read<ViewState>().selectedPassword;
      KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
      PasswordViewModel password =
          context.read<PasswordListViewModel>().getPasswordById(passwordID);
      Clipboard.setData(ClipboardData(
          text: keyChain.decrypt(password.cseKey, password.password)));
    }

    if ((shortcut.contains(LogicalKeyboardKey.controlLeft) ||
            shortcut.contains(LogicalKeyboardKey.controlRight)) &&
        shortcut.contains(LogicalKeyboardKey.keyB)) {
      String passwordID = context.read<ViewState>().selectedPassword;
      KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
      PasswordViewModel password =
          context.read<PasswordListViewModel>().getPasswordById(passwordID);
      Clipboard.setData(ClipboardData(
          text: keyChain.decrypt(password.cseKey, password.username)));
    }
  }

  Future<void> relogin(BuildContext context) async {
    await NextcloudProvider.handleRelogin(context);
  }

  Future<void> showClientPasswordDialog(BuildContext context) async {
    await ClientPasswordDialogue.showClientPasswordDialogue(context);
  }

  void _savePasswordSplitWeights(
      int childIndex1, int childIndex2, BuildContext context) {
    context.read<ViewState>().passwordSplitWeights =
        passwordSplitController!.weights.toList();
  }

  void _saveFolderSplitWeights(
      int childIndex1, int childIndex2, BuildContext context) {
    context.read<ViewState>().folderSplitWeights =
        folderSplitController!.weights.toList();
  }
}
