// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/provider/nextcloud_provider.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/widgets/themes_dropdown.dart';

Widget settingsDetailScreen(
    BuildContext context, settingsItems selectedSetting) {
  switch (selectedSetting) {
    case settingsItems.login:
      return TextButton(
        onPressed: () {
          context.read<ViewState>().handleLogout(context);
        },
        child: Text(AppLocalizations.of(context)!.logout),
      );
    case settingsItems.system:
      return Container(
        child: Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(AppLocalizations.of(context)!.syncRate),
              TextField(
                controller: TextFieldController.refreshRateController,
                keyboardType: TextInputType.number,
                showCursor: true,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              ),
              TextButton(
                  onPressed: () {
                    context.read<ConfigViewModel>().refreshRateInSeconds =
                        int.parse(
                            TextFieldController.refreshRateController.text);
                    NextcloudProvider.retrieveObjects(
                        context,
                        int.parse(
                            TextFieldController.refreshRateController.text));
                  },
                  child: Text(AppLocalizations.of(context)!.save))
            ],
          ),
        ),
        alignment: Alignment.center,
      );
    case settingsItems.themes:
      return const ThemesDropDown();

    case settingsItems.about:
      return Text(AppLocalizations.of(context)!.about);
  }
}
