// Dart imports:
import 'dart:convert';

// Package imports:
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/persistence_constants.dart';
import 'package:nextcloud_password_client/models/config_model.dart';
import 'package:nextcloud_password_client/models/credentials_model.dart';
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';

class DataAccessLayer {
  static late Box<ConfigModel> _configBox;
  static late Box<ServerConfigModel> _serverConfigBox;
  static late Box<FolderModel> _folderBox;
  static late Box<PasswordModel> _passwordBox;
  static late Box<CredentialsModel> _credentialBox;
  static late Box<ViewState> _viewStateBox;
  static late Box<TagModel> _tagBox;

  static Future<void> openBoxes() async {
    const FlutterSecureStorage secureStorage = FlutterSecureStorage();
    if (!await secureStorage.containsKey(key: 'key')) {
      await secureStorage.write(
          key: 'key', value: base64UrlEncode(Hive.generateSecureKey()));
    }
    var encryptionKey =
        base64Url.decode(await secureStorage.read(key: 'key') as String);

    _configBox = await Hive.openBox<ConfigModel>(persistenceConfigBox);
    _serverConfigBox =
        await Hive.openBox<ServerConfigModel>(persistenceServerConfigBox);
    _viewStateBox = await Hive.openBox<ViewState>(persistenceViewStateBox);
    _folderBox = await Hive.openBox<FolderModel>(persistenceFolderBox,
        encryptionCipher: HiveAesCipher(encryptionKey));
    _passwordBox = await Hive.openBox<PasswordModel>(persistencePasswordsBox,
        encryptionCipher: HiveAesCipher(encryptionKey));
    _tagBox = await Hive.openBox<TagModel>(persistenceTagsBox,
        encryptionCipher: HiveAesCipher(encryptionKey));
    _credentialBox =
        await Hive.openBox<CredentialsModel>(persistenceCredentialBox);
  }

  static ConfigModel? loadConfig() {
    return _configBox.containsKey(persistenceConfigKey)
        ? _configBox.get(persistenceConfigKey)
        : ConfigModel();
  }

  static void persistConfig(ConfigModel configModel) {
    _configBox.put(persistenceConfigKey, configModel);
  }

  static void persistServerConfig(ServerConfigModel serverConfigModel) {
    _serverConfigBox.put(persistenceServerConfigKey, serverConfigModel);
  }

  static ServerConfigModel? loadServerConfig() {
    return _serverConfigBox.containsKey(persistenceServerConfigKey)
        ? _serverConfigBox.get(persistenceServerConfigKey)
        : ServerConfigModel();
  }

  static ViewState? loadViewState() {
    return _viewStateBox.containsKey(persistenceViewStateKey)
        ? _viewStateBox.get(persistenceViewStateKey)
        : ViewState();
  }

  static void persistViewState(ViewState viewState) {
    _viewStateBox.put(persistenceViewStateKey, viewState);
  }

  static void persistFolder(List<FolderModel> folderModels) {
    for (var folder in folderModels) {
      _folderBox.put(folder.id, folder);
    }
  }

  static List<FolderModel> loadFolder() {
    return _folderBox.isEmpty ? [] : _folderBox.values.toList();
  }

  static void persistPasswords(List<PasswordModel> passwordModels) {
    for (var password in passwordModels) {
      _passwordBox.put(password.id, password);
    }
  }

  static void savePassword(PasswordModel password) {
    _passwordBox.put(password.id, password);
  }

  static List<PasswordModel> loadPasswords() {
    return _passwordBox.isEmpty ? [] : _passwordBox.values.toList();
  }

  static void persistCredentials(CredentialsModel credentialsModel) {
    _credentialBox.put(persistenceCredentialKey, credentialsModel);
  }

  static CredentialsModel? loadCredentials() {
    return _credentialBox.containsKey(persistenceCredentialKey)
        ? _credentialBox.get(persistenceCredentialKey)
        : CredentialsModel();
  }

  static void persistTags(List<TagModel> tagModels) {
    for (var tag in tagModels) {
      _tagBox.put(tag.id, tag);
    }
  }

  static List<TagModel> loadTags() {
    return _tagBox.isEmpty ? [] : _tagBox.values.toList();
  }

  static void closeBoxes() async {
    _viewStateBox.compact().then((value) => _viewStateBox.close());
    _folderBox.compact().then((value) => _folderBox.close());
    _passwordBox.compact().then((value) => _passwordBox.close());
    _credentialBox.compact().then((value) => _credentialBox.close());
    _serverConfigBox.compact().then((value) => _serverConfigBox.close());
    _configBox.compact().then((value) => _configBox.close());
  }

  static void deleteConfig() {
    _configBox.clear();
  }

  static void deleteCredentials() {
    _credentialBox.clear();
  }

  static void deleteViewState() {
    _viewStateBox.clear();
  }

  static void deletePasswords() {
    _passwordBox.clear();
  }

  static void deleteFolder() {
    _folderBox.clear();
  }

  static Future<void> handleLogout() async {
    deleteBoxes();
  }

  static void deleteBoxes() {
    deleteConfig();
    deleteCredentials();
    deleteViewState();
    deletePasswords();
    deleteFolder();
  }
}
