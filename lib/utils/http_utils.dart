// Dart imports:
import 'dart:convert';
import 'dart:io';

// Package imports:
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'package:url_launcher/url_launcher.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/http_constants.dart';

class HttpUtils {
  static int? _status = 0;
  static bool? _result = false;
  static bool? _isUrlValid = false;
  static String _url = '';
  static String _userName = '';
  static String _password = '';
  static String _session = '';

  static setLoginData(
      String url, String username, String password, String session) {
    _url = url;
    _userName = username;
    _password = password;
    _session = session;
  }

  static bool _certificateCheck(X509Certificate cert, String host, int port) =>
      true;
  static final IOClient _client = IOClient(HttpClient()
    ..badCertificateCallback = _certificateCheck
    ..maxConnectionsPerHost = 1);

  static Future<bool?> checkConn(String url) async {
    if (url.isNotEmpty) {
      await _client.get(Uri.parse(url)).then((response) {
        _status = response.statusCode;
        if (_status == null) {
          _result = false;
          return false;
        } else {
          _result = _status == 200;
          _status = null;
          return _result;
        }
      }).onError((dynamic error, stackTrace) {
        _status = null;
        _result = null;
        return _result;
      });
    }
    if (_status == null && (_result == false || _result == null)) {
      _isUrlValid = false;
      return false;
    } else {
      _isUrlValid = _result;
      _status = null;
      _result = null;
      return _isUrlValid;
    }
  }

  static Future<http.Response> httpGet(String _uri) async {
    return _client.get(Uri.parse(_url + _uri),
        headers: HttpUtils._getHeaders());
  }

  static Future<http.Response> httpPost(String _uri, {String? body}) async {
    return _client.post(Uri.parse(_url + _uri),
        headers: HttpUtils._getHeaders(), body: body);
  }

  static Future<http.Response> httpUpdate(String _uri, {String? body}) async {
    return _client.patch(Uri.parse(_url + _uri),
        headers: HttpUtils._getHeaders(), body: body);
  }

  static Map<String, String> _getHeaders({bool contentType = true}) {
    final _basicAuth = 'Basic ' +
        base64Encode(
            utf8.encode(HttpUtils._userName + ':' + HttpUtils._password));
    Map<String, String> _headers = {
      basicAuth: 'true',
      authorization: _basicAuth,
      if (_session != '') 'x-api-session': _session,
      if (contentType) 'Content-Type': 'application/json',
      if (contentType) 'accept': 'application/json'
    };
    return _headers;
  }

  static void openUrl(String _url) async {
    await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
  }
}
