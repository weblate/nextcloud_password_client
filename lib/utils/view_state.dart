// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';
import 'package:pluto_grid/pluto_grid.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/page_config_constants.dart';
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/ok_code.dart';
import 'package:nextcloud_password_client/enums/page_state.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/router/page_action.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'http_utils.dart';

part 'view_state.g.dart';

@HiveType(typeId: 6)
class ViewState extends ChangeNotifier {
  bool isURLReachable = false;
  OKCode _masterPasswordInputAnswer = OKCode.cancel;
  bool _isLoading = false;
  bool _isLoggedIn = false;
  bool _clientAuthenticated = false;
  PageAction _currentAction = PageAction(widget: null);
  bool _masterPasswordInputError = false;
  settingsItems _selectedSettingsItem = settingsItems.login;
  bool _clientPasswordInputError = false;
  bool _credentialInputError = false;
  @HiveField(0)
  Nodes _selectedNode = Nodes.all;
  @HiveField(1)
  String _selectedFolder = '';
  @HiveField(2)
  String _selectedTag = '';
  @HiveField(3)
  bool _sharedByYou = true;
  @HiveField(4)
  Security _selectedSecurity = Security.secure;
  @HiveField(5)
  String _selectedFavorite = '';
  @HiveField(6)
  String _selectedPassword = '';
  bool _ignoreMissingCredentials = false;
  bool _relogin = true;
  bool _obscurePassword = true;
  @HiveField(7)
  List<double> folderSplitWeights = [0.3, 0.7];
  @HiveField(8)
  List<double> passwordSplitWeights = [0.7, 0.3];
  @HiveField(9)
  PlutoColumn? sortedColumn;
  @HiveField(10)
  PlutoColumnSort? columnSort;
  bool inEditMode = false;

  ViewState();
  settingsItems get selectedSettingsItem => _selectedSettingsItem;
  bool get obscurePassword => _obscurePassword;
  PageAction get currentAction => _currentAction;
  bool get relogin => _relogin;
  bool get ignoreMissingCredentials => _ignoreMissingCredentials;
  bool get masterPasswordInputError => _masterPasswordInputError;
  bool get clientAuthenticated => _clientAuthenticated;
  bool get clientPasswordInputError => _clientPasswordInputError;
  bool get credentialInputError => _credentialInputError;
  bool get isLoading => _isLoading;
  bool get sharedByYou => _sharedByYou;
  bool get isLoggedIn => _isLoggedIn;
  OKCode get masterPasswordInputAnswer => _masterPasswordInputAnswer;
  String get selectedFolder => _selectedFolder;
  String get selectedPassword => _selectedPassword;
  Nodes get selectedNode => _selectedNode;
  String get selectedTag => _selectedTag;
  Security get selectedSecurity => _selectedSecurity;
  String get selectedFavorite => _selectedFavorite;

  set selectedSettingsItem(settingsItems selectedSettingsItem) {
    _selectedSettingsItem = selectedSettingsItem;
    notifyListeners();
  }

  set sharedByYou(bool sharedByYou) {
    _sharedByYou = sharedByYou;
    notifyListeners();
  }

  set selectedNode(Nodes node) {
    _selectedNode = node;
    notifyListeners();
  }

  set selectedTag(String tagId) {
    _selectedTag = tagId;
    notifyListeners();
  }

  set selectedSecurity(Security security) {
    _selectedSecurity = security;
    notifyListeners();
  }

  set selectedFavorite(String passwordID) {
    _selectedFavorite = passwordID;
    notifyListeners();
  }

  set selectedPassword(String passwordID) {
    _selectedPassword = passwordID;
    notifyListeners();
  }

  set relogin(bool relogin) {
    _relogin = relogin;
    notifyListeners();
  }

  set obscurePassword(bool obscure) {
    _obscurePassword = obscure;
    notifyListeners();
  }

  set ignoreMissingCredentials(bool ignore) {
    _ignoreMissingCredentials = ignore;
    notifyListeners();
  }

  set clientAuthenticated(bool authenticated) {
    _clientAuthenticated = authenticated;
    notifyListeners();
  }

  set selectedFolder(String folderID) {
    _selectedFolder = folderID;
    notifyListeners();
  }

  set masterPasswordInputError(bool inputError) {
    _masterPasswordInputError = inputError;
    notifyListeners();
  }

  set clientPasswordInputError(bool inputError) {
    _masterPasswordInputError = inputError;
    notifyListeners();
  }

  set credentialInputError(bool inputError) {
    _credentialInputError = inputError;
    notifyListeners();
  }

  set currentAction(PageAction action) {
    _currentAction = action;
    notifyListeners();
  }

  void resetCurrentAction() {
    _currentAction = PageAction(widget: const SizedBox());
  }

  Future<bool> checkURLreachability(String host) async {
    isURLReachable = (await HttpUtils.checkConn(host))!;
    notifyListeners();
    return isURLReachable;
  }

  set isLoading(bool isLoading) {
    _isLoading = isLoading;
    notifyListeners();
  }

  set masterPasswordInputAnswer(okCode) {
    _masterPasswordInputAnswer = okCode;
    notifyListeners();
  }

  set isLoggedIn(bool loggedIn) {
    _isLoggedIn = loggedIn;
    if (loggedIn) {
      showPasswordScreen();
    } else {
      showLoginScreen();
    }
  }

  void showLoginScreen() {
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: loginPageConfig,
      widget: const SizedBox(),
    );
    notifyListeners();
  }

  void showPasswordScreen() {
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: passwordScreenPageConfig,
      widget: const SizedBox(),
    );
    notifyListeners();
  }

  void persistViewState() {
    DataAccessLayer.persistViewState(this);
  }

  void showSettings() {
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: settingsPageConfig,
      widget: const SizedBox(),
    );
    notifyListeners();
  }

  Future<void> handleAppClosing() async {
    persistViewState();
    DataAccessLayer.closeBoxes();
  }

  Future<void> initialize() async {
    ViewState viewState = DataAccessLayer.loadViewState()!;
    _selectedNode = viewState.selectedNode;
    _selectedFolder = viewState.selectedFolder;
    _selectedTag = viewState.selectedTag;
    _sharedByYou = viewState.sharedByYou;
    _selectedSecurity = viewState.selectedSecurity;
    _selectedFavorite = viewState.selectedFavorite;
    _selectedPassword = viewState.selectedPassword;
    sortedColumn = viewState.sortedColumn;
    columnSort = viewState.columnSort;
    folderSplitWeights = viewState.folderSplitWeights;
    passwordSplitWeights = viewState.passwordSplitWeights;
  }

  void handleLogout(BuildContext context) {
    context.read<ConfigViewModel>().handleLogout();
    context.read<CredentialsViewModel>().handleLogout();
    context.read<FolderListViewModel>().handleLogout();
    context.read<PasswordListViewModel>().handleLogout();
    DataAccessLayer.handleLogout();
    resetViewState();
  }

  void resetViewState() {
    isURLReachable = false;
    _masterPasswordInputAnswer = OKCode.cancel;
    _isLoading = false;
    _clientAuthenticated = false;
    _masterPasswordInputError = false;
    _selectedSettingsItem = settingsItems.login;
    _clientPasswordInputError = false;
    _credentialInputError = false;
    _selectedFolder = '';
    _selectedPassword = '';
    _ignoreMissingCredentials = false;
    _relogin = true;
    _obscurePassword = true;
    folderSplitWeights = [0.3, 0.7];
    passwordSplitWeights = [0.7, 0.3];
    isLoggedIn = false;
  }
}
