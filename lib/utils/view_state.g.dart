// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'view_state.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ViewStateAdapter extends TypeAdapter<ViewState> {
  @override
  final int typeId = 6;

  @override
  ViewState read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ViewState()
      .._selectedNode = fields[0] as Nodes
      .._selectedFolder = fields[1] as String
      .._selectedTag = fields[2] as String
      .._sharedByYou = fields[3] as bool
      .._selectedSecurity = fields[4] as Security
      .._selectedFavorite = fields[5] as String
      .._selectedPassword = fields[6] as String
      ..folderSplitWeights = (fields[7] as List).cast<double>()
      ..passwordSplitWeights = (fields[8] as List).cast<double>()
      ..sortedColumn = fields[9] as PlutoColumn?
      ..columnSort = fields[10] as PlutoColumnSort?;
  }

  @override
  void write(BinaryWriter writer, ViewState obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj._selectedNode)
      ..writeByte(1)
      ..write(obj._selectedFolder)
      ..writeByte(2)
      ..write(obj._selectedTag)
      ..writeByte(3)
      ..write(obj._sharedByYou)
      ..writeByte(4)
      ..write(obj._selectedSecurity)
      ..writeByte(5)
      ..write(obj._selectedFavorite)
      ..writeByte(6)
      ..write(obj._selectedPassword)
      ..writeByte(7)
      ..write(obj.folderSplitWeights)
      ..writeByte(8)
      ..write(obj.passwordSplitWeights)
      ..writeByte(9)
      ..write(obj.sortedColumn)
      ..writeByte(10)
      ..write(obj.columnSort);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ViewStateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
