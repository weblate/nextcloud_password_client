// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/models/config_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class ConfigViewModel extends ConfigModel {
  ConfigModel _configModel = ConfigModel();

  Future<void> initialize() async {
    loadConfig();
    refreshViewModel();
  }

  ConfigViewModel();

  int geTemporaryID() {
    $temporaryID += 1;
    persistConfig();
    return $temporaryID;
  }

  @override
  Themes get selectedTheme => $selectedTheme;

  @override
  bool get saveCredentials => $saveCredentials;

  @override
  bool get saveMasterPassword => $saveMasterPassword;

  @override
  bool get useLocalCopy => $useLocalCopy;

  ConfigModel get configModel => _configModel;

  @override
  int get refreshRateInSeconds => $refreshRateInSeconds;

  set selectedTheme(Themes theme) {
    $selectedTheme = theme;
    notifyListeners();
  }

  set refreshRateInSeconds(int refreshRate) {
    $refreshRateInSeconds = refreshRate;
    notifyListeners();
  }

  set saveMasterPassword(saveMasterPassword) {
    $saveMasterPassword = saveMasterPassword;
    notifyListeners();
  }

  set useLocalCopy(useLocalCopy) {
    $useLocalCopy = useLocalCopy;
    notifyListeners();
  }

  set saveCredentials(saveCredentials) {
    $saveCredentials = saveCredentials;
    notifyListeners();
  }

  void refreshDataModel() {
    _configModel.serverURL = serverURL;
    _configModel.$useLocalCopy = $useLocalCopy;
    _configModel.masterPasswordRequired = masterPasswordRequired;
    _configModel.$saveCredentials = $saveCredentials;
    _configModel.$saveMasterPassword = $saveMasterPassword;
    _configModel.loginSucceeded = loginSucceeded;
    _configModel.session = session;
    _configModel.keyChain = keyChain;
    _configModel.$selectedTheme = $selectedTheme;
    _configModel.$refreshRateInSeconds = $refreshRateInSeconds;
  }

  void refreshViewModel() {
    serverURL = _configModel.serverURL;
    $useLocalCopy = _configModel.$useLocalCopy;
    masterPasswordRequired = _configModel.masterPasswordRequired;
    $saveCredentials = _configModel.saveCredentials;
    $saveMasterPassword = _configModel.$saveMasterPassword;
    loginSucceeded = _configModel.loginSucceeded;
    session = _configModel.session;
    keyChain = _configModel.keyChain;
    $selectedTheme = _configModel.$selectedTheme;
    $refreshRateInSeconds = _configModel.$refreshRateInSeconds;
  }

  void persistConfig() async {
    refreshDataModel();
    DataAccessLayer.persistConfig(_configModel);
  }

  void loadConfig() {
    _configModel = DataAccessLayer.loadConfig()!;
  }

  void handleLogout() {
    _configModel = ConfigModel();
    refreshViewModel();
  }
}
