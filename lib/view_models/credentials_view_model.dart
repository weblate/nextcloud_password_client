// Project imports:
import 'package:nextcloud_password_client/models/credentials_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class CredentialsViewModel extends CredentialsModel {
  CredentialsModel credentialsModel = CredentialsModel();

  CredentialsViewModel();

  Future<void> initialize() async {
    loadCredentials();
    refreshViewModel();
  }

  CredentialsModel get configModel => credentialsModel;

  void refreshDataModel() {
    credentialsModel.userName = userName;
    credentialsModel.password = password;
    credentialsModel.masterPassword = masterPassword;
    credentialsModel.clientPassword = clientPassword;
  }

  void refreshViewModel() {
    userName = credentialsModel.userName;
    password = credentialsModel.password;
    masterPassword = credentialsModel.masterPassword;
    clientPassword = credentialsModel.clientPassword;
  }

  void persistCredentials(bool saveCredentials, bool saveMasterPassword) {
    refreshDataModel();
    if (!saveCredentials) {
      credentialsModel.userName = '';
      credentialsModel.password = '';
    }
    if (!saveMasterPassword) {
      credentialsModel.masterPassword = '';
    }
    DataAccessLayer.persistCredentials(credentialsModel);
  }

  void loadCredentials() {
    credentialsModel = DataAccessLayer.loadCredentials()!;
  }

  void handleLogout() {
    credentialsModel = CredentialsModel();
    refreshViewModel();
  }
}
