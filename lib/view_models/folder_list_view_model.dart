// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';

class FolderListViewModel extends ChangeNotifier {
  Map<String, List<FolderViewModel>> _folderViewModels = {};

  FolderListViewModel();

  Map<String, List<FolderViewModel>> get folderViewModels => _folderViewModels;

  set folderViewModels(Map<String, List<FolderViewModel>> folderViewModels) {
    _folderViewModels = folderViewModels;
    notifyListeners();
  }

  Future<void> initialize() async {
    loadFolder();
  }

  void loadFolder() {
    List<FolderModel> models = DataAccessLayer.loadFolder();
    for (FolderModel folder in models) {
      if (!_folderViewModels.containsKey(folder.parent)) {
        _folderViewModels[folder.parent] = [];
      }
      _folderViewModels[folder.parent]!.add(FolderViewModel.fromModel(folder));
    }
    notifyListeners();
  }

  Future<void> persistFolder() async {
    List<FolderModel> models = [];
    for (List<FolderModel> folder in _folderViewModels.values) {
      models.addAll(folder);
    }
    DataAccessLayer.persistFolder(models);
  }

  FolderViewModel getFolderById(String folderID) {
    FolderViewModel model = FolderViewModel();
    _folderViewModels.forEach((key, value) {
      for (var element in value) {
        if (element.id == folderID) {
          model = element;
        }
      }
    });
    return model;
  }

  void handleLogout() {
    _folderViewModels.clear();
  }

  List<FolderViewModel> getFavoriteFolder() {
    List<FolderViewModel> models = [];
    for (List<FolderViewModel> folderViewModels in _folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        if (folderViewModel.favorite) {
          models.add(folderViewModel);
        }
      }
    }
    return models;
  }

  List<String> getParentFolder(String selectedFolder) {
    List<String> parents = [];
    for (List<FolderViewModel> folders in _folderViewModels.values) {
      for (FolderViewModel folder in folders) {
        if (folder.id == selectedFolder) {
          parents.addAll(getParentFolder(folder.parent));
          parents.add(selectedFolder);
        }
      }
    }
    return parents;
  }
}
