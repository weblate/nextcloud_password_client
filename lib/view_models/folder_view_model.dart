// Project imports:
import 'package:nextcloud_password_client/models/folder_model.dart';

class FolderViewModel extends FolderModel {
  FolderModel folderModel = FolderModel();

  FolderViewModel();

  FolderViewModel.fromModel(FolderModel model) {
    folderModel = model;
    refreshViewModel();
  }

  FolderViewModel.fromMap(Map<String, dynamic> folder) {
    folderModel = FolderModel();
    folderModel.id = id = folder['id'];
    folderModel.label = label = folder['label'];
    folderModel.parent = parent = folder['parent'];
    folderModel.revision = revision = folder['revision'];
    folderModel.cseType = cseType = folder['cseType'];
    folderModel.cseKey = cseKey = folder['cseKey'];
    folderModel.sseType = sseType = folder['sseType'];
    folderModel.client = client = folder['client'];
    folderModel.hidden = hidden = folder['hidden'];
    folderModel.trashed = trashed = folder['trashed'];
    folderModel.favorite = favorite = folder['favorite'];
    folderModel.created = created = folder['created'];
    folderModel.updated = updated = folder['updated'];
    folderModel.edited = edited = folder['edited'];
  }

  void refreshViewModel() {
    id = folderModel.id;
    label = folderModel.label;
    parent = folderModel.parent;
    revision = folderModel.revision;
    cseType = folderModel.cseType;
    cseKey = folderModel.cseKey;
    sseType = folderModel.sseType;
    client = folderModel.client;
    hidden = folderModel.hidden;
    trashed = folderModel.trashed;
    favorite = folderModel.favorite;
    created = folderModel.created;
    updated = folderModel.updated;
    edited = folderModel.edited;
  }

  void refreshModel() {
    folderModel.id = id;
    folderModel.label = label;
    folderModel.parent = parent;
    folderModel.revision = revision;
    folderModel.cseType = cseType;
    folderModel.cseKey = cseKey;
    folderModel.sseType = sseType;
    folderModel.client = client;
    folderModel.hidden = hidden;
    folderModel.trashed = trashed;
    folderModel.favorite = favorite;
    folderModel.created = created;
    folderModel.updated = updated;
    folderModel.edited = edited;
  }
}
