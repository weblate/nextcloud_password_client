// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pluto_grid/pluto_grid.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_row.dart';

class PasswordListViewModel extends ChangeNotifier {
  late PlutoGridStateManager plutoStateManager;

  Map<String, List<PasswordViewModel>> _passwordViewModels = {};
  List<PasswordViewModel> _filteredPasswordViewModels = [];
  List<PlutoRow> _passwordRows = [];

  PasswordListViewModel();

  List<PasswordViewModel> get filteredPasswordViewModels =>
      _filteredPasswordViewModels;

  Map<String, List<PasswordViewModel>> get passwordViewModels =>
      _passwordViewModels;

  set passwordViewModels(
      Map<String, List<PasswordViewModel>> passwordViewModels) {
    _passwordViewModels = passwordViewModels;
    notifyListeners();
  }

  void setFavoritePasswords(BuildContext context) {
    plutoStateManager.removeRows(_passwordRows);
    _passwordRows = [];
    _filteredPasswordViewModels = [];
    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        if (passwordViewModel.favorite) {
          _filteredPasswordViewModels.add(passwordViewModel);
        }
      }
    }
    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setPasswordsByFolder(BuildContext context, String folderID) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];
    if (folderID.isEmpty || folderID == apiRootFolder) {
      for (var passwordViewModels in passwordViewModels.values) {
        _filteredPasswordViewModels.addAll(passwordViewModels);
        _passwordRows.addAll(getPasswordRows(context, passwordViewModels));
      }
    } else {
      if (_passwordViewModels.containsKey(folderID)) {
        _filteredPasswordViewModels.addAll(_passwordViewModels[folderID]!);
        _passwordRows
            .addAll(getPasswordRows(context, _passwordViewModels[folderID]!));
      }
    }
    resetPlutoGrid(context);
  }

  Future<void> initialize() async {
    loadPasswords();
  }

  void loadPasswords() {
    List<PasswordModel> models = DataAccessLayer.loadPasswords();
    for (PasswordModel password in models) {
      if (!_passwordViewModels.containsKey(password.folder)) {
        _passwordViewModels[password.folder] = [];
      }
      _passwordViewModels[password.folder]!
          .add(PasswordViewModel.fromModel(password));
    }
    notifyListeners();
  }

  Future<void> persistPasswords() async {
    List<PasswordModel> models = [];
    for (List<PasswordViewModel> viewModels in _passwordViewModels.values) {
      for (PasswordViewModel password in viewModels) {
        models.add(password.passwordModel);
      }
    }
    DataAccessLayer.persistPasswords(models);
  }

  PasswordViewModel getPasswordById(String passwordID) {
    PasswordViewModel model = PasswordViewModel();
    if (passwordID.isEmpty) {
      if (_passwordViewModels.isNotEmpty) {
        model = _passwordViewModels.values.first.first;
      }
    } else {
      _passwordViewModels.forEach((folderID, passwordList) {
        for (var password in passwordList) {
          if (password.id == passwordID) {
            model = password;
          }
        }
      });
    }
    return model;
  }

  void handleLogout() {
    _passwordViewModels.clear();
  }

  void setPasswordsByTag(BuildContext context, String id) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];
    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        if (passwordViewModel.tags.contains(id)) {
          _filteredPasswordViewModels.add(passwordViewModel);
        }
      }
    }
    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setPasswordsByShare(BuildContext context, bool sharedByYou) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];
    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        for (PasswordShareViewModel share
            in passwordViewModel.shareViewModels) {
          if (sharedByYou) {
            if (share.owner['name'] ==
                context.read<CredentialsViewModel>().userName) {
              if (!_filteredPasswordViewModels.contains(passwordViewModel)) {
                _filteredPasswordViewModels.add(passwordViewModel);
              }
            }
          } else {
            if (share.receiver['name'] ==
                context.read<CredentialsViewModel>().userName) {
              if (!_filteredPasswordViewModels.contains(passwordViewModel)) {
                _filteredPasswordViewModels.add(passwordViewModel);
              }
            }
          }
        }
      }
    }
    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setSecurePasswords(BuildContext context, Security security) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];
    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        switch (security) {
          case Security.secure:
            if (passwordViewModel.status == 0) {
              _filteredPasswordViewModels.add(passwordViewModel);
            }
            break;

          case Security.weak:
            if (passwordViewModel.status == 1) {
              _filteredPasswordViewModels.add(passwordViewModel);
            }
            break;

          case Security.breached:
            if (passwordViewModel.status == 2) {
              _filteredPasswordViewModels.add(passwordViewModel);
            }
            break;
        }
      }
    }
    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void resetPlutoGrid(BuildContext context) {
    plutoStateManager.appendRows(_passwordRows);
    if (plutoStateManager.getSortedColumn != null) {
      switch (plutoStateManager.getSortedColumn!.sort) {
        case PlutoColumnSort.none:
          break;
        case PlutoColumnSort.ascending:
          plutoStateManager.sortAscending(plutoStateManager.getSortedColumn!);
          break;
        case PlutoColumnSort.descending:
          plutoStateManager.sortDescending(plutoStateManager.getSortedColumn!);
          break;
      }
    }
    plutoStateManager.notifyListeners();
  }

  void setRecentPasswords(BuildContext context) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];
    List<PasswordViewModel> sortedPasswordViewModels = [];
    for (var passwordViewModels in passwordViewModels.values) {
      sortedPasswordViewModels.addAll(passwordViewModels);
    }
    sortedPasswordViewModels.sort();
    for (int i = 1; i <= 15; i++) {
      _filteredPasswordViewModels.add(sortedPasswordViewModels[i]);
    }
    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setPasswordsInitially(BuildContext context) {
    ViewState viewState = context.read<ViewState>();
    switch (viewState.selectedNode) {
      case Nodes.all:
        setPasswordsByFolder(context, apiRootFolder);
        break;
      case Nodes.folder:
        setPasswordsByFolder(context, viewState.selectedFolder);
        break;
      case Nodes.recent:
        setRecentPasswords(context);
        break;
      case Nodes.favorite:
        if (viewState.selectedFolder.isNotEmpty) {
          setPasswordsByFolder(context, viewState.selectedFolder);
        } else if (viewState.selectedTag.isNotEmpty) {
          setPasswordsByTag(context, viewState.selectedTag);
        } else {
          setFavoritePasswords(context);
        }
        break;
      case Nodes.shared:
        setPasswordsByShare(context, viewState.sharedByYou);
        break;
      case Nodes.tag:
        setPasswordsByTag(context, viewState.selectedTag);
        break;
      case Nodes.security:
        setSecurePasswords(context, viewState.selectedSecurity);
        break;
    }
  }

  PlutoRow? getRowByPassword(String selectedPassword) {
    PlutoRow? plutoRow;
    for (PlutoRow row in _passwordRows) {
      if (row.key.toString() == selectedPassword) {
        plutoRow = row;
      }
    }
    return plutoRow;
  }
}
