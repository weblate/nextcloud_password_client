// Project imports:
import 'package:nextcloud_password_client/models/password_revision_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';

class PasswordRevisionViewModel extends PasswordRevisionModel {
  PasswordRevisionModel passwordRevisionModel = PasswordRevisionModel();
  PasswordRevisionViewModel.fromModel(PasswordRevisionModel model) {
    passwordRevisionModel = model;
    refreshViewModel();
  }

  PasswordRevisionViewModel.fromMap(Map<String, dynamic> revision) {
    passwordRevisionModel.id = id = revision['id'];
    passwordRevisionModel.label = label = revision['label'];
    passwordRevisionModel.username = username = revision['username'];
    passwordRevisionModel.password = password = revision['password'];
    passwordRevisionModel.url = url = revision['url'];
    passwordRevisionModel.notes = notes = revision['notes'];
    passwordRevisionModel.customFields =
        customFields = revision['customFields'];
    passwordRevisionModel.status = status = revision['status'];
    passwordRevisionModel.statusCode = statusCode = revision['statusCode'];
    passwordRevisionModel.hash = hash = revision['hash'];
    passwordRevisionModel.folder = folder = revision['folder'];
    passwordRevisionModel.cseType = cseType = revision['cseType'];
    passwordRevisionModel.cseKey = cseKey = revision['cseKey'];
    passwordRevisionModel.sseType = sseType = revision['sseType'];
    passwordRevisionModel.client = client = revision['client'];
    passwordRevisionModel.hidden = hidden = revision['hidden'];
    passwordRevisionModel.trashed = trashed = revision['trashed'];
    passwordRevisionModel.favorite = favorite = revision['favorite'];
    passwordRevisionModel.edited = edited = revision['edited'];
    passwordRevisionModel.created = created = revision['created'];
    passwordRevisionModel.updated = updated = revision['updated'];
  }

  void refreshViewModel() {
    id = passwordRevisionModel.id;
    label = passwordRevisionModel.label;
    username = passwordRevisionModel.username;
    password = passwordRevisionModel.password;
    url = passwordRevisionModel.url;
    notes = passwordRevisionModel.notes;
    customFields = passwordRevisionModel.customFields;
    status = passwordRevisionModel.status;
    statusCode = passwordRevisionModel.statusCode;
    hash = passwordRevisionModel.hash;
    folder = passwordRevisionModel.folder;
    cseType = passwordRevisionModel.cseType;
    cseKey = passwordRevisionModel.cseKey;
    sseType = passwordRevisionModel.sseType;
    client = passwordRevisionModel.client;
    hidden = passwordRevisionModel.hidden;
    trashed = passwordRevisionModel.trashed;
    favorite = passwordRevisionModel.favorite;
    edited = passwordRevisionModel.edited;
    created = passwordRevisionModel.created;
    updated = passwordRevisionModel.updated;
  }

  refreshModel() {
    passwordRevisionModel.id = id;
    passwordRevisionModel.label = label;
    passwordRevisionModel.username = username;
    passwordRevisionModel.password = password;
    passwordRevisionModel.url = url;
    passwordRevisionModel.notes = notes;
    passwordRevisionModel.customFields = customFields;
    passwordRevisionModel.status = status;
    passwordRevisionModel.statusCode = statusCode;
    passwordRevisionModel.hash = hash;
    passwordRevisionModel.folder = folder;
    passwordRevisionModel.cseType = cseType;
    passwordRevisionModel.cseKey = cseKey;
    passwordRevisionModel.sseType = sseType;
    passwordRevisionModel.client = client;
    passwordRevisionModel.hidden = hidden;
    passwordRevisionModel.trashed = trashed;
    passwordRevisionModel.favorite = favorite;
    passwordRevisionModel.edited = edited;
    passwordRevisionModel.created = created;
    passwordRevisionModel.updated = updated;
  }

  PasswordRevisionViewModel.fromPasswordModel(
      PasswordViewModel passwordViewModel) {
    passwordRevisionModel.id = id = passwordViewModel.id;
    passwordRevisionModel.label = label = passwordViewModel.label;
    passwordRevisionModel.username = username = passwordViewModel.username;
    passwordRevisionModel.password = password = passwordViewModel.password;
    passwordRevisionModel.url = url = passwordViewModel.url;
    passwordRevisionModel.notes = notes = passwordViewModel.notes;
    passwordRevisionModel.customFields =
        customFields = passwordViewModel.customFields;
    passwordRevisionModel.status = status = passwordViewModel.status;
    passwordRevisionModel.statusCode =
        statusCode = passwordViewModel.statusCode;
    passwordRevisionModel.hash = hash = passwordViewModel.hash;
    passwordRevisionModel.folder = folder = passwordViewModel.folder;
    passwordRevisionModel.cseType = cseType = passwordViewModel.cseType;
    passwordRevisionModel.cseKey = cseKey = passwordViewModel.cseKey;
    passwordRevisionModel.sseType = sseType = passwordViewModel.sseType;
    passwordRevisionModel.client = client = passwordViewModel.client;
    passwordRevisionModel.hidden = hidden = passwordViewModel.hidden;
    passwordRevisionModel.trashed = trashed = passwordViewModel.trashed;
    passwordRevisionModel.favorite = favorite = passwordViewModel.favorite;
    passwordRevisionModel.edited = edited = passwordViewModel.edited;
    passwordRevisionModel.created = created = passwordViewModel.created;
    passwordRevisionModel.updated = updated = passwordViewModel.updated;
  }
}
