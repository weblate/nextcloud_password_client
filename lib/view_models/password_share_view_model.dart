// Project imports:
import 'package:nextcloud_password_client/models/password_share_model.dart';

class PasswordShareViewModel extends PasswordShareModel {
  PasswordShareModel passwordShareModel = PasswordShareModel();

  PasswordShareViewModel.fromModel(PasswordShareModel model) {
    passwordShareModel = model;
    refreshViewModel();
  }
  PasswordShareViewModel();
  PasswordShareViewModel.fromMap(Map<String, dynamic> share) {
    passwordShareModel.id = id = share['id'];
    share['expires'] == null
        ? passwordShareModel.expires = expires = 000000000000000
        : passwordShareModel.expires = expires = share['expires'];
    passwordShareModel.editable = editable = share['editable'];
    passwordShareModel.password = password = share['password'];
    passwordShareModel.shareable = shareable = share['shareable'];
    passwordShareModel.updatePending = updatePending = share['updatePending'];
    passwordShareModel.owner = owner = share['owner'];
    passwordShareModel.receiver = receiver = share['receiver'];
    passwordShareModel.created = created = share['created'];
    passwordShareModel.updated = updated = share['updated'];
  }

  void refreshViewModel() {
    id = passwordShareModel.id;
    password = passwordShareModel.password;
    created = passwordShareModel.created;
    updated = passwordShareModel.updated;
    expires = passwordShareModel.expires;
    editable = passwordShareModel.editable;
    shareable = passwordShareModel.shareable;
    updatePending = passwordShareModel.updatePending;
    owner = passwordShareModel.owner;
    receiver = passwordShareModel.receiver;
  }

  refreshModel() {
    passwordShareModel.id = id;
    passwordShareModel.expires;
    passwordShareModel.editable;
    passwordShareModel.password;
    passwordShareModel.shareable;
    passwordShareModel.updatePending;
    passwordShareModel.owner;
    passwordShareModel.receiver;
    passwordShareModel.created;
    passwordShareModel.updated;
  }
}
