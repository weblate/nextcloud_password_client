// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/models/password_revision_model.dart';
import 'package:nextcloud_password_client/models/password_share_model.dart';
import 'package:nextcloud_password_client/models/view_model_interface.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_revision_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class PasswordViewModel extends PasswordModel
    implements Comparable<PasswordViewModel>, ViewModelInterface {
  List<PasswordRevisionViewModel> revisionViewModels = [];
  List<PasswordShareViewModel> shareViewModels = [];
  PasswordModel passwordModel = PasswordModel();
  PasswordShareViewModel passwordShare = PasswordShareViewModel();
  List<dynamic> customFieldMap = [];

  PasswordViewModel();

  PasswordViewModel.fromModel(PasswordModel model) {
    passwordModel = model;
    refreshViewModel();
  }

  PasswordViewModel.fromMap(Map<String, dynamic> password) {
    passwordModel.id = id = password['id'];
    passwordModel.label = label = password['label'];
    passwordModel.username = username = password['username'];
    passwordModel.password = this.password = password['password'];
    passwordModel.url = url = password['url'];
    passwordModel.notes = notes = password['notes'];
    passwordModel.customFields = customFields = password['customFields'];
    passwordModel.status = status = password['status'];
    passwordModel.statusCode = statusCode = password['statusCode'];
    passwordModel.hash = hash = password['hash'];
    passwordModel.folder = folder = password['folder'];
    passwordModel.revision = revision = password['revision'];
    if (password['share'] != null) {
      passwordShare = PasswordShareViewModel.fromMap(password['share']);
      passwordModel.share = passwordShare.passwordShareModel;
    }

    passwordModel.shared = shared = password['shared'];
    passwordModel.cseType = cseType = password['cseType'];
    passwordModel.cseKey = cseKey = password['cseKey'];
    passwordModel.sseType = sseType = password['sseType'];
    passwordModel.client = client = password['client'];
    passwordModel.hidden = hidden = password['hidden'];
    passwordModel.trashed = trashed = password['trashed'];
    passwordModel.favorite = favorite = password['favorite'];
    passwordModel.editable = editable = password['editable'];
    passwordModel.edited = edited = password['edited'];
    passwordModel.created = created = password['created'];
    passwordModel.updated = updated = password['updated'];
    revisionViewModels = getRevisionsFromAPI(password['revisions']);
    passwordModel.revisions = getRevisionModelFromViewModels();
    passwordModel.tags = tags = getTagIdsFromAPI(password['tags']);
    shareViewModels = getSharesFromAPI(password['shares']);
    passwordModel.shares = getShareModelFromViewModels();
  }

  void refreshViewModel() {
    id = passwordModel.id;
    label = passwordModel.label;
    username = passwordModel.username;
    password = passwordModel.password;
    url = passwordModel.url;
    notes = passwordModel.notes;
    customFields = passwordModel.customFields;
    status = passwordModel.status;
    statusCode = passwordModel.statusCode;
    hash = passwordModel.hash;
    folder = passwordModel.folder;
    revision = passwordModel.revision;
    share = PasswordShareViewModel.fromModel(passwordModel.share);
    shared = passwordModel.shared;
    cseType = passwordModel.cseType;
    cseKey = passwordModel.cseKey;
    sseType = passwordModel.sseType;
    client = passwordModel.client;
    hidden = passwordModel.hidden;
    trashed = passwordModel.trashed;
    favorite = passwordModel.favorite;
    editable = passwordModel.editable;
    edited = passwordModel.edited;
    created = passwordModel.created;
    updated = passwordModel.updated;
    revisionViewModels = getRevisionViewModelFromModel(passwordModel);
    tags = passwordModel.tags;
    shareViewModels = getShareViewModelFromModel(passwordModel);
  }

  refreshModel() {
    passwordModel.id = id;
    passwordModel.label = label;
    passwordModel.username = username;
    passwordModel.password = password;
    passwordModel.url = url;
    passwordModel.notes = notes;
    passwordModel.customFields = customFields;
    passwordModel.status = status;
    passwordModel.statusCode = statusCode;
    passwordModel.hash = hash;
    passwordModel.folder = folder;
    passwordModel.revision = revision;
    passwordModel.share = passwordShare.passwordShareModel;
    passwordModel.shared = shared;
    passwordModel.cseType = cseType;
    passwordModel.cseKey = cseKey;
    passwordModel.sseType = sseType;
    passwordModel.client = client;
    passwordModel.hidden = hidden;
    passwordModel.trashed = trashed;
    passwordModel.favorite = favorite;
    passwordModel.editable = editable;
    passwordModel.edited = edited;
    passwordModel.created = created;
    passwordModel.updated = updated;
    passwordModel.revisions = getRevisionModelFromViewModels();
    passwordModel.tags = tags;
    passwordModel.shares = getShareModelFromViewModels();
  }

  @override
  Future<void> setFavorite(BuildContext context, bool newFavorite) async {
    favorite = newFavorite;
    passwordModel.favorite = favorite;
    DataAccessLayer.savePassword(passwordModel);
    notifyListeners();
    http.Response _passwordUpdateResponse = await HttpUtils.httpUpdate(
        apiPasswordUpdate,
        body: getBodyForFavoriteUpdate());
    if (_passwordUpdateResponse.statusCode == 200) {
      var body = json.decode(_passwordUpdateResponse.body);
      PasswordRevisionViewModel revision =
          PasswordRevisionViewModel.fromPasswordModel(this);
      revision.id = body['revision'];
      revision.favorite = !revision.favorite;
      revisionViewModels.add(revision);
      revisions.add(revision.passwordRevisionModel);
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError,
          duration: const Duration(seconds: 5), context: context);
    }

    notifyListeners();
  }

  List<PasswordRevisionViewModel> getRevisionsFromAPI(revisions) {
    List<PasswordRevisionViewModel> models = [];
    for (Map<String, dynamic> revision in revisions) {
      models.add(PasswordRevisionViewModel.fromMap(revision));
    }
    return models;
  }

  List<PasswordRevisionModel> getRevisionModelFromViewModels() {
    List<PasswordRevisionModel> models = [];
    for (PasswordRevisionViewModel revision in revisionViewModels) {
      models.add(revision.passwordRevisionModel);
    }
    return models;
  }

  List<PasswordRevisionViewModel> getRevisionViewModelFromModel(
      PasswordModel passwordModel) {
    List<PasswordRevisionViewModel> models = [];
    for (PasswordRevisionModel revision in passwordModel.revisions) {
      models.add(PasswordRevisionViewModel.fromModel(revision));
    }
    return models;
  }

  List<String> getTagIdsFromAPI(tags) {
    List<String> tagLIST = [];
    for (String tag in tags) {
      tagLIST.add(tag);
    }
    return tagLIST;
  }

  List<PasswordShareViewModel> getSharesFromAPI(shares) {
    List<PasswordShareViewModel> models = [];
    for (Map<String, dynamic> share in shares) {
      models.add(PasswordShareViewModel.fromMap(share));
    }
    return models;
  }

  List<PasswordShareModel> getShareModelFromViewModels() {
    List<PasswordShareModel> models = [];
    for (PasswordShareViewModel share in shareViewModels) {
      models.add(share.passwordShareModel);
    }
    return models;
  }

  List<PasswordShareViewModel> getShareViewModelFromModel(
      PasswordModel passwordModel) {
    List<PasswordShareViewModel> models = [];
    for (PasswordShareModel share in passwordModel.shares) {
      models.add(PasswordShareViewModel.fromModel(share));
    }
    return models;
  }

  @override
  int compareTo(PasswordViewModel other) {
    if (edited < other.edited) {
      return 1;
    } else if (edited > other.edited) {
      return -1;
    } else {
      return 0;
    }
  }

  String getBodyForFavoriteUpdate() {
    return jsonEncode({
      'id': id,
      'password': password,
      'label': label,
      'hash': hash,
      'cseType': cseType,
      'cseKey': cseKey,
      'favorite': favorite
    });
  }

  Future<void> deleteTag(
      BuildContext context, TagViewModel tagViewModel) async {
    tags.remove(tagViewModel.id);
    passwordModel.tags = tags;
    DataAccessLayer.savePassword(passwordModel);
    notifyListeners();
    context.read<PasswordListViewModel>().notifyListeners();
    http.Response _passwordUpdateResponse = await HttpUtils.httpUpdate(
        apiPasswordUpdate,
        body: getBodyForTagUpdate());
    if (_passwordUpdateResponse.statusCode == 200) {
      var body = json.decode(_passwordUpdateResponse.body);
      PasswordRevisionViewModel revision =
          PasswordRevisionViewModel.fromPasswordModel(this);
      revision.id = body['revision'];
      revisionViewModels.add(revision);
      revisions.add(revision.passwordRevisionModel);
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError,
          duration: const Duration(seconds: 5), context: context);
    }

    notifyListeners();
  }

  String getBodyForTagUpdate() {
    return jsonEncode({
      'id': id,
      'password': password,
      'label': label,
      'hash': hash,
      'cseType': cseType,
      'cseKey': cseKey,
      'tags': tags.isEmpty ? [''] : tags
    });
  }

  Future<void> addTag(BuildContext context, String newTag) async {
    context.read<ViewState>().inEditMode = true;
    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
    PasswordListViewModel passwordList = context.read<PasswordListViewModel>();
    TagListViewModel tagListViewModel = context.read<TagListViewModel>();
    String encryptedTag =
        keyChain.encrypt(newTag, keyChain.type, keyChain.current);
    TagViewModel tag = tagListViewModel.getTagByLabel(encryptedTag);
    if (tag.label == encryptedTag) {
      tags.add(tag.id);
      passwordList.notifyListeners();
      notifyListeners();
      updatePassword(context);
    } else {
      TagViewModel tagTemp = TagViewModel();
      tagTemp.label = encryptedTag;
      tagTemp.id = context.read<ConfigViewModel>().geTemporaryID().toString();
      tagTemp.refreshModel();
      tagListViewModel.addTag(tagTemp);
      tags.add(tagTemp.id);
      passwordList.notifyListeners();
      notifyListeners();
      DataAccessLayer.savePassword(passwordModel);
      tagListViewModel.createTag(context, tagTemp).then((newTag) {
        tags.remove(tagTemp.id);
        tags.add(newTag.id);
        tagListViewModel.deleteTag(tagTemp);
        updatePassword(context);
      });
    }
  }

  Future<void> updatePassword(BuildContext context) async {
    http.Response response = await HttpUtils.httpUpdate(apiPasswordUpdate,
        body: getBodyForTagUpdate());
    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      PasswordRevisionViewModel revision =
          PasswordRevisionViewModel.fromPasswordModel(this);
      revision.id = body['revision'];
      revisionViewModels.add(revision);
      revisions.add(revision.passwordRevisionModel);
      notifyListeners();
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError,
          duration: const Duration(seconds: 5), context: context);
    }
    context.read<ViewState>().inEditMode = false;
    DataAccessLayer.savePassword(passwordModel);
  }
}
