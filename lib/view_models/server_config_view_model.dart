// Project imports:
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class ServerConfigViewModel extends ServerConfigModel {
  ServerConfigModel _serverConfigModel = ServerConfigModel();

  set serverConfigModel(ServerConfigModel serverConfigModel) {
    _serverConfigModel = serverConfigModel;
    refreshViewModel();
    notifyListeners();
  }

  Future<void> initialize() async {
    loadConfig();
    refreshViewModel();
  }

  ServerConfigViewModel();

  void refreshDataModel() {
    _serverConfigModel.userPasswordGeneratorStrength =
        userPasswordGeneratorStrength;
    _serverConfigModel.userPasswordGeneratorNumbers =
        userPasswordGeneratorNumbers;
    _serverConfigModel.userPasswordGeneratorSpecial =
        userPasswordGeneratorSpecial;
    _serverConfigModel.usePasswordSecurityDuplicates =
        usePasswordSecurityDuplicates;
    _serverConfigModel.usePasswordSecurityAge = usePasswordSecurityAge;
    _serverConfigModel.userPasswordSecurityHash = userPasswordSecurityHash;
    _serverConfigModel.useMailSecurity = useMailSecurity;
    _serverConfigModel.userMailShares = userMailShares;
    _serverConfigModel.userNotificationSecurity = userNotificationSecurity;
    _serverConfigModel.userNotificationShares = userNotificationShares;
    _serverConfigModel.userNotificationErrors = userNotificationErrors;
    _serverConfigModel.userNotificationAdmin = userNotificationAdmin;
    _serverConfigModel.userEncryptionSse = userEncryptionSse;
    _serverConfigModel.userEncryptionCse = userEncryptionCse;
    _serverConfigModel.userSharingEditable = userSharingEditable;
    _serverConfigModel.userSharingResharing = userSharingResharing;
    _serverConfigModel.userSessionLifetime = userSessionLifetime;
    _serverConfigModel.serverVersion = serverVersion;
    _serverConfigModel.serverAppVersion = serverAppVersion;
    _serverConfigModel.serverBaseUrl = serverBaseUrl;
    _serverConfigModel.serverBaseUrlWebdav = serverBaseUrlWebdav;
    _serverConfigModel.serverSharingEnabled = serverSharingEnabled;
    _serverConfigModel.serverSharingResharing = serverSharingResharing;
    _serverConfigModel.serverSharingAutocomplete = serverSharingAutocomplete;
    _serverConfigModel.serverSharingTypes = serverSharingTypes;
    _serverConfigModel.serverThemeColorPrimary = serverThemeColorPrimary;
    _serverConfigModel.serverThemeColorText = serverThemeColorText;
    _serverConfigModel.serverThemeColorBackground = serverThemeColorBackground;
    _serverConfigModel.serverThemeBackground = serverThemeBackground;
    _serverConfigModel.serverThemeLogo = serverThemeLogo;
    _serverConfigModel.serverThemeLabel = serverThemeLabel;
    _serverConfigModel.serverThemeAppIcon = serverThemeAppIcon;
    _serverConfigModel.serverThemeFolderIcon = serverThemeFolderIcon;
    _serverConfigModel.serverHandbookUrl = serverHandbookUrl;
    _serverConfigModel.serverPerformance = serverPerformance;
  }

  void refreshViewModel() {
    userPasswordGeneratorStrength =
        _serverConfigModel.userPasswordGeneratorStrength;
    userPasswordGeneratorNumbers =
        _serverConfigModel.userPasswordGeneratorNumbers;
    userPasswordGeneratorSpecial =
        _serverConfigModel.userPasswordGeneratorSpecial;
    usePasswordSecurityDuplicates =
        _serverConfigModel.usePasswordSecurityDuplicates;
    usePasswordSecurityAge = _serverConfigModel.usePasswordSecurityAge;
    userPasswordSecurityHash = _serverConfigModel.userPasswordSecurityHash;
    useMailSecurity = _serverConfigModel.useMailSecurity;
    userMailShares = _serverConfigModel.userMailShares;
    userNotificationSecurity = _serverConfigModel.userNotificationSecurity;
    userNotificationShares = _serverConfigModel.userNotificationShares;
    userNotificationErrors = _serverConfigModel.userNotificationErrors;
    userNotificationAdmin = _serverConfigModel.userNotificationAdmin;
    userEncryptionSse = _serverConfigModel.userEncryptionSse;
    userEncryptionCse = _serverConfigModel.userEncryptionCse;
    userSharingEditable = _serverConfigModel.userSharingEditable;
    userSharingResharing = _serverConfigModel.userSharingResharing;
    userSessionLifetime = _serverConfigModel.userSessionLifetime;
    serverVersion = _serverConfigModel.serverVersion;
    serverAppVersion = _serverConfigModel.serverAppVersion;
    serverBaseUrl = _serverConfigModel.serverBaseUrl;
    serverBaseUrlWebdav = _serverConfigModel.serverBaseUrlWebdav;
    serverSharingEnabled = _serverConfigModel.serverSharingEnabled;
    serverSharingResharing = _serverConfigModel.serverSharingResharing;
    serverSharingAutocomplete = _serverConfigModel.serverSharingAutocomplete;
    serverSharingTypes = _serverConfigModel.serverSharingTypes;
    serverThemeColorPrimary = _serverConfigModel.serverThemeColorPrimary;
    serverThemeColorText = _serverConfigModel.serverThemeColorText;
    serverThemeColorBackground = _serverConfigModel.serverThemeColorBackground;
    serverThemeBackground = _serverConfigModel.serverThemeBackground;
    serverThemeLogo = _serverConfigModel.serverThemeLogo;
    serverThemeLabel = _serverConfigModel.serverThemeLabel;
    serverThemeAppIcon = _serverConfigModel.serverThemeAppIcon;
    serverThemeFolderIcon = _serverConfigModel.serverThemeFolderIcon;
    serverHandbookUrl = _serverConfigModel.serverHandbookUrl;
    serverPerformance = _serverConfigModel.serverPerformance;
  }

  void persistConfig() async {
    refreshDataModel();
    DataAccessLayer.persistServerConfig(_serverConfigModel);
  }

  void loadConfig() {
    _serverConfigModel = DataAccessLayer.loadServerConfig()!;
  }

  void handleLogout() {
    _serverConfigModel = ServerConfigModel();
    refreshViewModel();
  }
}
