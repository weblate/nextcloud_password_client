// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'config_view_model.dart';

class TagListViewModel extends ChangeNotifier {
  List<TagViewModel> _tagViewModels = [];

  TagListViewModel();

  List<TagViewModel> get tagViewModels => _tagViewModels;

  set tagViewModels(List<TagViewModel> tagViewModels) {
    _tagViewModels = tagViewModels;
    notifyListeners();
  }

  Future<void> initialize() async {
    loadTags();
  }

  void loadTags() {
    List<TagModel> models = DataAccessLayer.loadTags();
    for (TagModel tag in models) {
      tagViewModels.add(TagViewModel.fromModel(tag));
    }
    notifyListeners();
  }

  Future<void> persistTags() async {
    List<TagModel> models = [];
    for (TagViewModel tag in tagViewModels) {
      models.add(tag.tagModel);
    }
    DataAccessLayer.persistTags(models);
  }

  TagViewModel getTagByLabel(String tagLabel) {
    TagViewModel model = TagViewModel();
    for (TagViewModel tag in _tagViewModels) {
      if (tag.label == tagLabel) {
        model = tag;
        break;
      }
    }
    return model;
  }

  TagViewModel getTagById(String tagID) {
    TagViewModel model = TagViewModel();
    for (TagViewModel tag in _tagViewModels) {
      if (tag.id == tagID) {
        model = tag;
        break;
      }
    }
    return model;
  }

  void handleLogout() {
    _tagViewModels.clear();
  }

  List<TagViewModel> getFavoriteTags() {
    List<TagViewModel> tagViewModels = [];
    for (TagViewModel tagViewModel in _tagViewModels) {
      if (tagViewModel.favorite) {
        tagViewModels.add(tagViewModel);
      }
    }
    return tagViewModels;
  }

  Future<TagViewModel> createTag(BuildContext context, TagViewModel tag) async {
    TagViewModel newTag = TagViewModel();
    http.Response _tagCreationResponse = await HttpUtils.httpPost(apiTagCreate,
        body: getBodyForTagCreation(context, tag.label));
    if (_tagCreationResponse.statusCode == 201) {
      var body = jsonDecode(_tagCreationResponse.body);
      newTag = await showTag(context, body['id']);
      if (newTag.id == body['id']) {
        _tagViewModels.add(newTag);
        notifyListeners();
        persistTags();
      }
    }
    return newTag;
  }

  String getBodyForTagCreation(BuildContext context, String encryptedTag) {
    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

    return jsonEncode({
      'label': encryptedTag,
      'cseType': keyChain.type,
      'cseKey': keyChain.current,
      'color': "#ffffff"
    });
  }

  Future<TagViewModel> showTag(BuildContext context, String tagId) async {
    TagViewModel newTag = TagViewModel();
    http.Response _tagShowResponse =
        await HttpUtils.httpPost(apiTagShow, body: jsonEncode({"id": tagId}));
    if (_tagShowResponse.statusCode == 200) {
      newTag = TagViewModel.fromMap(jsonDecode(_tagShowResponse.body));
    }
    return newTag;
  }

  void addTag(TagViewModel tag) {
    _tagViewModels.add(tag);
  }

  void deleteTag(TagViewModel tag) {
    _tagViewModels.remove(tag);
  }
}
