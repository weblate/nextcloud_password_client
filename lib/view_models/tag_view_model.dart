// Project imports:
import 'package:nextcloud_password_client/models/tag_model.dart';

class TagViewModel extends TagModel {
  TagModel tagModel = TagModel();

  TagViewModel();

  TagViewModel.fromModel(TagModel model) {
    tagModel = model;
    refreshViewModel();
  }

  TagViewModel.fromMap(Map<String, dynamic> tag) {
    tagModel = TagModel();
    tagModel.id = id = tag['id'];
    tagModel.label = label = tag['label'];
    tagModel.revision = revision = tag['revision'];
    tagModel.cseType = cseType = tag['cseType'];
    tagModel.cseKey = cseKey = tag['cseKey'];
    tagModel.sseType = sseType = tag['sseType'];
    tagModel.client = client = tag['client'];
    tagModel.color = tag['color'];
    tagModel.hidden = hidden = tag['hidden'];
    tagModel.trashed = trashed = tag['trashed'];
    tagModel.favorite = favorite = tag['favorite'];
    tagModel.created = created = tag['created'];
    tagModel.updated = updated = tag['updated'];
    tagModel.edited = edited = tag['edited'];
    refreshViewModel();
  }

  void refreshViewModel() {
    id = tagModel.id;
    label = tagModel.label;
    revision = tagModel.revision;
    cseType = tagModel.cseType;
    cseKey = tagModel.cseKey;
    sseType = tagModel.sseType;
    client = tagModel.client;
    color = tagModel.color;
    hidden = tagModel.hidden;
    trashed = tagModel.trashed;
    favorite = tagModel.favorite;
    created = tagModel.created;
    updated = tagModel.updated;
    edited = tagModel.edited;
  }

  void refreshModel() {
    tagModel.id = id;
    tagModel.label = label;
    tagModel.revision = revision;
    tagModel.cseType = cseType;
    tagModel.cseKey = cseKey;
    tagModel.sseType = sseType;
    tagModel.client = client;
    tagModel.color = color;
    tagModel.hidden = hidden;
    tagModel.trashed = trashed;
    tagModel.favorite = favorite;
    tagModel.created = created;
    tagModel.updated = updated;
    tagModel.edited = edited;
  }
}
