// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/enums/ok_code.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:nextcloud_password_client/widgets/single_elements/password_input.dart';

class MasterPasswordDialogue {
  static Future<void> showMasterPasswordDialogue(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    PasswordText(
                        TextFieldController.masterPasswordFieldController,
                        labelText:
                            AppLocalizations.of(context)!.enterMasterPassword,
                        errorText:
                            context.read<ViewState>().masterPasswordInputError
                                ? AppLocalizations.of(context)!
                                    .masterPasswordException
                                : null),
                    Row(
                      children: <Widget>[
                        Label(AppLocalizations.of(context)!.saveMasterPassword),
                        _SaveCredentialCheckBox(),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.decline),
              onPressed: () {
                context.read<ViewState>().masterPasswordInputError = true;
                _handleDialogButtonPress(context, OKCode.cancel);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.accept),
              onPressed: () {
                _handleDialogButtonPress(context, OKCode.accept);
              },
            )
          ],
        );
      },
    );
  }
}

void _handleDialogButtonPress(BuildContext context, okCode) {
  if (okCode == OKCode.accept) {
    context.read<CredentialsViewModel>().masterPassword =
        TextFieldController.masterPasswordFieldController.text;
  }
  context.read<ViewState>().masterPasswordInputAnswer = okCode;
  Navigator.of(context).pop();
}

class _SaveCredentialCheckBox extends StatefulWidget {
  @override
  _SaveCredentialCheckBoxState createState() => _SaveCredentialCheckBoxState();
}

class _SaveCredentialCheckBoxState extends State<_SaveCredentialCheckBox> {
  @override
  Widget build(BuildContext context) {
    bool _saveMasterPassword =
        context.watch<ConfigViewModel>().saveMasterPassword;
    return Checkbox(
      checkColor: Colors.white,
      value: _saveMasterPassword,
      onChanged: (value) {
        context.read<ConfigViewModel>().saveMasterPassword =
            !_saveMasterPassword;
      },
    );
  }
}
