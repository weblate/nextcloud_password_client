// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/widgets/menu/selection_handler.dart';

Widget getHelpMenu(BuildContext context) {
  return PopupMenuButton<String>(
    child: Text(AppLocalizations.of(context)!.helpMenu),
    tooltip: AppLocalizations.of(context)!.helpMenuTooltip,
    onSelected: (value) => handleMenuItemSelection(context, value),
    itemBuilder: (context) => <PopupMenuEntry<String>>[
      PopupMenuItem<String>(
        value: onlineHelp,
        child: ListTile(
          leading: const Icon(Icons.help),
          title: Text(AppLocalizations.of(context)!.onlineHelpMenuItem),
        ),
      ),
      PopupMenuItem<String>(
        value: donate,
        child: ListTile(
          leading: const Icon(Icons.monetization_on),
          title: Text(AppLocalizations.of(context)!.donationMenuItem),
        ),
      ),
      PopupMenuItem<String>(
        value: reportBug,
        child: ListTile(
          leading: const Icon(Icons.bug_report),
          title: Text(AppLocalizations.of(context)!.reportIssueMenuItem),
        ),
      ),
      PopupMenuItem<String>(
        value: about,
        child: ListTile(
          leading: const Icon(Icons.info),
          title: Text(AppLocalizations.of(context)!.aboutMenuItem),
        ),
      ),
    ],
  );
}
