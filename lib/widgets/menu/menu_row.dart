// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/widgets/menu/help_menu.dart';
import 'package:nextcloud_password_client/widgets/menu/system_settings.dart';

Widget getMenuBar(BuildContext context) {
  return Row(
    children: [
      Container(
        child: getSystemMenu(context),
        margin: const EdgeInsets.symmetric(horizontal: 5),
      ),
      Container(
        child: getHelpMenu(context),
        margin: const EdgeInsets.symmetric(horizontal: 5),
      ),
    ],
  );
}

openMenu(BuildContext context, String value) {
  context.read<ViewState>().showSettings();
}
