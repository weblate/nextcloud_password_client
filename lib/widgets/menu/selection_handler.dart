// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';

void handleMenuItemSelection(BuildContext context, String value) {
  switch (value) {
    case settings:
      context.read<ViewState>().showSettings();
      break;
    case onlineHelp:
      HttpUtils.openUrl(
          'https://gitlab.com/j0chn/nextcloud-password-client/-/wikis/home');
      break;
    case donate:
      HttpUtils.openUrl('https://www.ko-fi.com/j0chn');
      break;
    case reportBug:
      HttpUtils.openUrl(
          'https://gitlab.com/j0chn/nextcloud-password-client/-/issues');
      break;
    case about:
  }
}
