// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_simple_treeview/flutter_simple_treeview.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/constants/widget_constants.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'package:nextcloud_password_client/widgets/favorite_button.dart';

Widget createTree(BuildContext context) {
  final TreeController _treeController =
      TreeController(allNodesExpanded: false);
  final ScrollController _detailScrollController = ScrollController();
  final ScrollController _detailScrollController2 = ScrollController();
  SingleChildScrollView singleChildScrollView = SingleChildScrollView(
    controller: _detailScrollController,
    scrollDirection: Axis.vertical,
    child: SingleChildScrollView(
      controller: _detailScrollController2,
      scrollDirection: Axis.horizontal,
      child: TreeView(
        iconSize: 20,
        treeController: _treeController,
        nodes: _createAllNodes(
          context.read<FolderListViewModel>().folderViewModels,
          apiRootFolder,
          context,
        ),
      ),
    ),
  );
  ViewState viewState = context.read<ViewState>();
  FolderListViewModel folderListViewModel = context.read<FolderListViewModel>();
  List<String> parentFolders = [];
  switch (viewState.selectedNode) {
    case Nodes.folder:
      parentFolders =
          folderListViewModel.getParentFolder(viewState.selectedFolder);
      _treeController.expandNode(const ValueKey(apiRootFolder));
      for (String folder in parentFolders) {
        _treeController.expandNode(ValueKey(folder));
      }
      break;
    case Nodes.favorite:
      _treeController.expandNode(const ValueKey('favorite'));
      if (viewState.selectedFolder.isNotEmpty) {
        parentFolders =
            folderListViewModel.getParentFolder(viewState.selectedFolder);
        _treeController
            .expandNode(ValueKey(viewState.selectedFolder + 'favorite'));
      } else if (viewState.selectedTag.isNotEmpty) {
        _treeController
            .expandNode(ValueKey(viewState.selectedTag + 'favorite'));
      }
      break;
    case Nodes.shared:
      _treeController.expandNode(const ValueKey("shares"));
      break;
    case Nodes.tag:
      _treeController.expandNode(const ValueKey('tags'));
      break;
    case Nodes.security:
      _treeController.expandNode(const ValueKey('security'));
      break;
    case Nodes.all:
      break;
    case Nodes.recent:
      break;
  }
  return singleChildScrollView;
}

Widget _nodeRow(Icon icon, String text, BuildContext context,
    {FavoriteIcon? favIcon}) {
  return Row(children: [
    favIcon ?? const SizedBox.shrink(),
    icon,
    const SizedBox(width: 5),
    Text(text),
    PopupMenuButton<String>(
        onSelected: (value) =>
            showToast("not yet implemented", context: context),
        itemBuilder: (context) => <PopupMenuEntry<String>>[
              const PopupMenuItem<String>(
                value: 'nothing',
                child: ListTile(
                  title: Text('placeholder'),
                ),
              ),
            ]),
  ], mainAxisSize: MainAxisSize.min);
}

List<TreeNode> _createAllNodes(
    Map<String, List<FolderViewModel>> folderViewModels,
    String apiRootFolder,
    BuildContext context) {
  List<TreeNode> allNodes = [];
  allNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = apiRootFolder;
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.all;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setPasswordsByFolder(context, apiRootFolder);
        },
        child: _nodeRow(const Icon(MdiIcons.earth),
            AppLocalizations.of(context)!.all, context),
      ),
      key: const ValueKey("all"),
    ),
  );
  allNodes
      .addAll(_createFolders(folderViewModels, apiRootFolder, context, false));
  allNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.recent;
          viewState.persistViewState();
          context.read<PasswordListViewModel>().setRecentPasswords(context);
        },
        child: _nodeRow(const Icon(Icons.timelapse_rounded),
            AppLocalizations.of(context)!.recent, context),
      ),
      key: const ValueKey("recent"),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getFavoriteNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(const Icon(Icons.star),
            AppLocalizations.of(context)!.favorites, context),
      ),
      key: const ValueKey("favorite"),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getShareNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(const Icon(Icons.share),
            AppLocalizations.of(context)!.shared, context),
      ),
      key: const ValueKey("shares"),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getTagNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(const Icon(MdiIcons.tagMultiple),
            AppLocalizations.of(context)!.tags, context),
      ),
      key: const ValueKey("tags"),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getSecurityNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(const Icon(MdiIcons.shieldHalfFull),
            AppLocalizations.of(context)!.security, context),
      ),
      key: const ValueKey("security"),
    ),
  );

  return allNodes;
}

_getSecurityNodes(BuildContext context) {
  List<TreeNode> treeNodes = [];
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.security;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setSecurePasswords(context, Security.secure);
        },
        child: _nodeRow(
            const Icon(MdiIcons.shieldHalfFull, color: Colors.green),
            AppLocalizations.of(context)!.secure,
            context),
      ),
      key: const ValueKey("secure"),
    ),
  );
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.weak;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.security;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setSecurePasswords(context, Security.weak);
        },
        child: _nodeRow(
            const Icon(MdiIcons.shieldHalfFull, color: Colors.amber),
            AppLocalizations.of(context)!.weak,
            context),
      ),
      key: const ValueKey("weak"),
    ),
  );
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.breached;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.security;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setSecurePasswords(context, Security.breached);
        },
        child: _nodeRow(const Icon(MdiIcons.shieldHalfFull, color: Colors.red),
            AppLocalizations.of(context)!.breached, context),
      ),
      key: const ValueKey("breached"),
    ),
  );
  return treeNodes;
}

_getTagNodes(BuildContext context) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
  List<TreeNode> treeNodes = [];
  for (TagViewModel tagViewModel
      in context.read<TagListViewModel>().tagViewModels) {
    treeNodes.add(
      TreeNode(
        content: MaterialButton(
          onPressed: () {
            ViewState viewState = context.read<ViewState>();
            viewState.selectedFolder = '';
            viewState.selectedPassword = '';
            viewState.selectedFavorite = '';
            viewState.sharedByYou = true;
            viewState.selectedSecurity = Security.secure;
            viewState.selectedTag = tagViewModel.id;
            viewState.selectedNode = Nodes.tag;
            viewState.persistViewState();
            context
                .read<PasswordListViewModel>()
                .setPasswordsByTag(context, tagViewModel.id);
          },
          child: _nodeRow(
              Icon(
                MdiIcons.tag,
                color: Color(
                  int.parse(
                    keyChain
                        .decrypt(tagViewModel.cseKey, tagViewModel.color)
                        .replaceFirst(colorHashTag, colorPrefix),
                  ),
                ),
              ),
              keyChain.decrypt(tagViewModel.cseKey, tagViewModel.label),
              context),
        ),
        key: ValueKey(tagViewModel.id),
      ),
    );
  }
  return treeNodes;
}

List<TreeNode> _getShareNodes(BuildContext context) {
  List<TreeNode> treeNodes = [];
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          context
              .read<PasswordListViewModel>()
              .setPasswordsByShare(context, false);
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = false;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.shared;
          viewState.persistViewState();
        },
        child: _nodeRow(const Icon(Icons.share),
            AppLocalizations.of(context)!.sharedWithYou, context),
      ),
      key: const ValueKey("shared with you"),
    ),
  );
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          context
              .read<PasswordListViewModel>()
              .setPasswordsByShare(context, true);
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.shared;
          viewState.persistViewState();
        },
        child: _nodeRow(const Icon(Icons.share),
            AppLocalizations.of(context)!.sharedByYou, context),
      ),
      key: const ValueKey("shared by you"),
    ),
  );
  return treeNodes;
}

List<TreeNode> _getFavoriteNodes(BuildContext context) {
  List<TreeNode> treeNodes = [];
  List<FolderViewModel> folderViewModels =
      context.read<FolderListViewModel>().getFavoriteFolder();
  List<TagViewModel> tagViewModels =
      context.read<TagListViewModel>().getFavoriteTags();
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.favorite;
          viewState.persistViewState();
          context.read<PasswordListViewModel>().setFavoritePasswords(context);
        },
        child: _nodeRow(const Icon(Icons.vpn_key),
            AppLocalizations.of(context)!.passwords, context),
      ),
      key: const ValueKey('favorite_passwords'),
    ),
  );
  for (FolderViewModel folderViewModel in folderViewModels) {
    treeNodes.addAll(_createFolders({}, folderViewModel.id, context, true));
  }

  for (TagViewModel tagViewModel in tagViewModels) {
    treeNodes.add(
      TreeNode(
        content: MaterialButton(
          onPressed: () {
            ViewState viewState = context.read<ViewState>();
            viewState.selectedFolder = '';
            viewState.selectedPassword = '';
            viewState.selectedFavorite = '';
            viewState.sharedByYou = true;
            viewState.selectedSecurity = Security.secure;
            viewState.selectedTag = tagViewModel.id;
            viewState.selectedNode = Nodes.favorite;
            viewState.persistViewState();
            context
                .read<PasswordListViewModel>()
                .setPasswordsByTag(context, tagViewModel.id);
          },
          child: _nodeRow(
              Icon(
                MdiIcons.tag,
                color: Color(
                  int.parse(
                    keyChain
                        .decrypt(tagViewModel.cseKey, tagViewModel.color)
                        .replaceFirst(colorHashTag, colorPrefix),
                  ),
                ),
              ),
              keyChain.decrypt(tagViewModel.cseKey, tagViewModel.label),
              context,
              favIcon: FavoriteIcon(tagViewModel)),
        ),
        key: ValueKey(tagViewModel.id + "favorite"),
      ),
    );
  }
  return treeNodes;
}

List<TreeNode> _createFolders(Map<String, List<FolderViewModel>> folderMap,
    String currentFolder, BuildContext context, bool favorite) {
  List<TreeNode> currentLevelNodes = [];
  if (folderMap.containsKey(currentFolder)) {
    for (var folder in folderMap[currentFolder]!) {
      currentLevelNodes.addAll(
        _createFolders(folderMap, folder.id, context, favorite),
      );
    }
  }
  FolderViewModel folder =
      context.read<FolderListViewModel>().getFolderById(currentFolder);
  if (folder.id.isEmpty) {
    folder.id = apiRootFolder;
    folder.label = AppLocalizations.of(context)!.folder;
  }
  return [
    TreeNode(
      children: currentLevelNodes,
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = currentFolder;
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.folder;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setPasswordsByFolder(context, currentFolder);
        },
        child: _nodeRow(
            const Icon(Icons.folder),
            context
                .read<ConfigViewModel>()
                .keyChain
                .decrypt(folder.cseKey, folder.label),
            context,
            favIcon: folder.id != apiRootFolder
                ? FavoriteIcon(folder)
                : null),
      ),
      key: favorite ? ValueKey(folder.id + 'favorite') : ValueKey(folder.id),
    )
  ];
}
