// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/view_models/server_config_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:nextcloud_password_client/widgets/single_elements/password_input.dart';

Widget generalPasswordDetails(
    BuildContext context, PasswordViewModel password) {
  ConfigViewModel configViewModel = context.read<ConfigViewModel>();
  TextEditingController passwordController =
      TextFieldController.passwordController;
  passwordController.text =
      configViewModel.keyChain.decrypt(password.cseKey, password.password);
  List<Widget> customFields = _getCustomFields(
      password,
      configViewModel.keyChain,
      context.read<ServerConfigViewModel>().serverBaseUrlWebdav);
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        AppLocalizations.of(context)!.general,
        style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
      Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Label(AppLocalizations.of(context)!.usernameColumn + ':'),
          Label(configViewModel.keyChain
              .decrypt(password.cseKey, password.username)),
        ],
      ),
      Row(
        children: [
          Label(AppLocalizations.of(context)!.passwordColumn),
          const SizedBox(width: 10, height: 30),
          SizedBox(
              width: 400, height: 70, child: PasswordText(passwordController)),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Label(AppLocalizations.of(context)!.urlColumn),
          const SizedBox(width: 10, height: 30),
          InkWell(
              child: Text(
                configViewModel.keyChain.decrypt(password.cseKey, password.url),
                style: const TextStyle(color: Colors.blueAccent),
              ),
              onTap: () => HttpUtils.openUrl(configViewModel.keyChain
                  .decrypt(password.cseKey, password.url))),
        ],
      ),
      for (Widget customField in customFields) customField
    ],
  );
}

List<Widget> _getCustomFields(
    PasswordViewModel password, KeyChain keyChain, String webdavUrl) {
  List<Widget> customFields = [];
  if (password.customFields.isNotEmpty) {
    List<dynamic> customFieldList =
        jsonDecode(keyChain.decrypt(password.cseKey, password.customFields));
    for (dynamic field in customFieldList) {
      String label = field['label'];
      String value = field['value'];
      switch (field['type']) {
        case 'secret':
          /*TextEditingController secretController = TextEditingController();
        customFields.add(Row(children: [
          Text(label),
          PasswordText(secretController, value, '')
        ]));*/
          break;
        case 'email':
          customFields.add(
            Row(
              children: [
                Text(label),
                InkWell(
                    child: Text(
                      value,
                      style: const TextStyle(color: Colors.blueAccent),
                    ),
                    onTap: () => HttpUtils.openUrl('mailto:' + value))
              ],
            ),
          );
          break;
        case 'url':
          customFields.add(
            Row(
              children: [
                Text(label),
                InkWell(
                    child: Text(
                      field['value'],
                      style: const TextStyle(color: Colors.blueAccent),
                    ),
                    onTap: () => HttpUtils.openUrl(value))
              ],
            ),
          );
          break;
        case 'file':
          customFields.add(
            Row(
              children: [
                Text(label),
                InkWell(
                    child: Text(
                      field['value'],
                      style: const TextStyle(color: Colors.blueAccent),
                    ),
                    onTap: () => HttpUtils.openUrl(webdavUrl + value))
              ],
            ),
          );
          break;
        case 'data':
          break;
        case 'text':
        default:
          customFields.add(Row(children: [Text(label), Text(value)]));
          break;
      }
    }
  }
  return customFields;
}
