// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

Widget serverConnection(BuildContext context) {
  if (context.read<ConfigViewModel>().serverURL.isNotEmpty) {
    TextFieldController.urlTextFieldController.text =
        context.read<ConfigViewModel>().serverURL.substring(8);
  }
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(
        child: getGeneralTextInput(TextFieldController.urlTextFieldController,
            label: AppLocalizations.of(context)!.enterURL, prefix: apiProtocol),
      ),
      Container(
        child: const ConnectionCheckButton(),
        height: 50,
        width: 50,
        margin: const EdgeInsets.only(left: 10),
      ),
    ],
  );
}

class ConnectionCheckButton extends StatefulWidget {
  static bool _loading = false;

  const ConnectionCheckButton({Key? key}) : super(key: key);

  @override
  _ConnectionCheckButtonState createState() => _ConnectionCheckButtonState();
}

class _ConnectionCheckButtonState extends State<ConnectionCheckButton> {
  String _toolTip = '';
  Icon _icon = const Icon(Icons.offline_bolt);

  @override
  Widget build(BuildContext context) {
    _toolTip = AppLocalizations.of(context)!.checkURLReachability;
    return ConnectionCheckButton._loading
        ? const CircularProgressIndicator()
        : FloatingActionButton(
            heroTag: 'serverConnectionButton',
            onPressed: () async => _checkConnection(),
            tooltip: _toolTip,
            child: _icon,
          );
  }

  _checkConnection() async {
    ConnectionCheckButton._loading = true;
    ConfigViewModel configViewModel = context.read<ConfigViewModel>();
    setState(() {});
    configViewModel.serverURL =
        apiProtocol + TextFieldController.urlTextFieldController.text;
    bool result = await context
        .read<ViewState>()
        .checkURLreachability(configViewModel.serverURL);
    ConnectionCheckButton._loading = false;
    if (result) {
      _toolTip = AppLocalizations.of(context)!.serverReached;
      _icon = const Icon(Icons.bolt, color: Colors.green);
    } else {
      _toolTip = AppLocalizations.of(context)!.serverNotReached;
      _icon = const Icon(Icons.bolt, color: Colors.red);
    }
    setState(() {});
  }
}
