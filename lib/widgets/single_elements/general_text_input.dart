// Flutter imports:
import 'package:flutter/material.dart';

Widget getGeneralTextInput(TextEditingController textController,
    {String? label,
    String? prefix,
    String? error,
    bool? obscure,
    VoidCallback? onEditingCompleted}) {
  return TextField(
    controller: textController,
    showCursor: true,
    maxLength: 50,
    decoration: getInputDecoration(label: label, prefix: prefix, error: error),
    onEditingComplete: onEditingCompleted,
  );
}

InputDecoration getInputDecoration({
  String? label,
  String? prefix,
  String? error,
  Widget? suffixIcon,
  bool? isDense,
}) {
  return InputDecoration(
    labelText: label,
    prefixText: prefix,
    errorText: error,
    border: const UnderlineInputBorder(),
    isDense: isDense,
    suffixIcon: suffixIcon,
  );
}

class GeneralTextInput extends StatefulWidget {
  final TextEditingController textEditingController;
  final String? labelText;
  final String? prefixText;
  final String? errorText;
  final VoidCallback? onEditingCompleted;
  const GeneralTextInput(this.textEditingController,
      {Key? key,
      this.labelText,
      this.prefixText,
      this.errorText,
      this.onEditingCompleted})
      : super(key: key);

  @override
  GeneralTextInputState createState() => GeneralTextInputState();
}

class GeneralTextInputState extends State<GeneralTextInput> {
  @override
  Widget build(BuildContext context) {
    return GeneralTextField(
      widget.textEditingController,
      GeneralInputDeco(
          labelText: widget.labelText,
          prefixText: widget.prefixText,
          errorText: widget.errorText),
      onEditingComplete: widget.onEditingCompleted,
    );
  }
}

class GeneralInputDeco extends InputDecoration {
  const GeneralInputDeco({labelText = '', prefixText = '', errorText = ''})
      : super(
          labelText: labelText,
          prefixText: prefixText,
          errorText: errorText,
          border: const UnderlineInputBorder(),
        );
}

class GeneralTextField extends TextField {
  const GeneralTextField(
      TextEditingController controller, InputDecoration decoration,
      {VoidCallback? onEditingComplete, Key? key})
      : super(
            showCursor: true,
            maxLength: 50,
            onEditingComplete: onEditingComplete,
            decoration: decoration,
            controller: controller,
            key: key);
}
