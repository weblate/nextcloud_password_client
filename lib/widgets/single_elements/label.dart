// Flutter imports:
import 'package:flutter/cupertino.dart';

class Label extends Text {
  const Label(String text, {Key? key}) : super(text, key: key);
}
