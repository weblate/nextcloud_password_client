// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

class PasswordText extends GeneralTextInput {
  const PasswordText(textEditingController, {key, labelText, errorText})
      : super(textEditingController,
            key: key, labelText: labelText, errorText: errorText);

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends GeneralTextInputState {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return PasswordTextField(
      widget.textEditingController,
      _obscureText,
      PasswordInputDeco(
          IconButton(
            onPressed: () {
              _obscureText = !_obscureText;
              setState(
                () {},
              );
            },
            icon: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
          ),
          true,
          widget.errorText,
          widget.labelText),
    );
  }
}

class PasswordInputDeco extends GeneralInputDeco {
  const PasswordInputDeco(suffixIcon, isDense, errorText, labelText)
      : super(labelText: labelText, errorText: errorText);
}

class PasswordTextField extends GeneralTextField {
  const PasswordTextField(controller, obscureText, decoration, {Key? key})
      : super(controller, decoration, key: key);
}
