// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';

class ThemesDropDown extends StatefulWidget {
  const ThemesDropDown({Key? key}) : super(key: key);

  @override
  _ThemesDropDownState createState() => _ThemesDropDownState();
}

class _ThemesDropDownState extends State<ThemesDropDown> {
  Themes selectedTheme = Themes.nextcloud;
  @override
  Widget build(BuildContext context) {
    selectedTheme = context.read<ConfigViewModel>().selectedTheme;
    return DropdownButton(
        value: selectedTheme,
        items: [
          DropdownMenuItem(
            child: Text(AppLocalizations.of(context)!.dark),
            value: Themes.dark,
          ),
          DropdownMenuItem(
            child: Text(AppLocalizations.of(context)!.light),
            value: Themes.light,
          ),
          const DropdownMenuItem(
            child: Text("Nextcloud"),
            value: Themes.nextcloud,
          ),
          /*DropdownMenuItem(
            child: Text("System"),
            value: Themes.system,
          ),*/
          DropdownMenuItem(
            child: Text(AppLocalizations.of(context)!.custom),
            value: Themes.custom,
          ),
        ],
        onChanged: (value) {
          setState(() {
            selectedTheme = value as Themes;
            context.read<ConfigViewModel>().selectedTheme = selectedTheme;
          });
        });
  }
}
