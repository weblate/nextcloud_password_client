// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';

void main() {
  test('Refresh FolderModel', () {
    FolderViewModel folder = FolderViewModel();
    folder.id = 'Test';
    folder.refreshModel();
    expect(folder.id, folder.folderModel.id);
  });
}
